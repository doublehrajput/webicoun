// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
export const API_BASE_URL = 'https://webicounindia.herokuapp.com/';
export const SERVER_BASE_URL = 'https://webicoun.com/admin/index.php';
//
// export const API_BASE_URL = 'https://webicoun-backend-hajkn.run-ap-south1.goorm.io/';

export const RAZORPAY_OPTIONS = {
  key: 'rzp_live_auqqGG95tspfBQ',
  amount: '',
  name: 'Webicoun',
  order_id: '',
  description: 'Load Wallet',
  image: 'https://livestatic.novopay.in/resources/img/nodeapp/img/Logo_NP.jpg',
  modal: {},
  theme: {
    color: '#0096C5',
  },
};

export const firebaseConfig = {
  apiKey: 'AIzaSyCXJA9yvWbwhvFTiUXFY3lPsEmZa-9KqVs',
  authDomain: 'webicoun.firebaseapp.com',
  databaseURL: 'https://webicoun.firebaseio.com',
  projectId: 'webicoun',
  storageBucket: 'webicoun.appspot.com',
  messagingSenderId: '196198283217',
  appId: '1:196198283217:web:87e7f2e6c7d236430a92f0',
};

/* export const categories = {
  "mental-wellness-coach" : "Mental Wellness Coach",
  "emotional-wellness-coach" : "Emotional Wellness Coach",
  "physical-wellness-trainers": "Physical Wellness Trainers",
  "nutritionist-health-expert" : "Nutritionist/Health Expert",
  "motivational-speaker" : "Motivational Speaker",
  "business-coach": "Business Coach",
  "personality-development-coach": "Personality Development Coach",
  "career-counsellor" : "Career Counsellor",
  "environmentalist": "Environmentalist",
  "etiquette-coach":"Etiquette Coach",
  "spiritual-speakers": "Spiritual Speakers",
  "happiness-coach": "Happiness Coach",
};

*/

export const categories = {
  "yoga" : "Yoga",
  "career-counsellor":"Career",
  "mental-wellness":"Mental Wellness",
  "emotional-wellness":"Emotional Wellness",
  "personality-development": "Personality Development",
  "happiness-coach": "Happiness",
  "meditation" : "Meditation",
  "motivational-speaker" : "Motivation",
 
  
 };