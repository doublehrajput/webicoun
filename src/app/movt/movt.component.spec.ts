import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovtComponent } from './movt.component';

describe('MovtComponent', () => {
  let component: MovtComponent;
  let fixture: ComponentFixture<MovtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
