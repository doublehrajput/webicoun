import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Expertsignup2Component } from './expertsignup2.component';

describe('Expertsignup2Component', () => {
  let component: Expertsignup2Component;
  let fixture: ComponentFixture<Expertsignup2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Expertsignup2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Expertsignup2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
