import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  HttpClientModule,
  HttpClient,
  HttpRequest,
  HttpResponse,
  HttpEventType,
} from '@angular/common/http';
import { Routes, RouterModule, ActivatedRoute, Router } from '@angular/router';
// import dictionary from '../expertsignup/expertsignup.component';
import { ExpertsignupComponent } from '../expertsignup/expertsignup.component';
import { RegisterServiceService } from '../register-service.service';
import { ExpertsignupsService } from '../expertsignups.service';

@Component({
  selector: 'app-expertsignup2',
  templateUrl: './expertsignup2.component.html',
  styleUrls: ['./expertsignup2.component.css'],
})
export class Expertsignup2Component implements OnInit {
  @ViewChild('panCard') panCard: any;
  @ViewChild('adharCard') adharCard: any;
  @ViewChild('cancelCheque') cancelCheque: any;
  successMessage;
  url = 'https://webicounindia.herokuapp.com/expert/register-expert2/1233'; //id from previos form respone
  registerForm2: FormGroup;
  submitted = false;
  selectedFile: File;
  fileToUpload: File = null;
  passid = { id: '' };
  constructor(
    private formBuilder: FormBuilder,
    private myservice: RegisterServiceService,
    private http: HttpClient,
    private router: Router,
    private expertService: ExpertsignupsService
  ) {
    // this.passid=this.expertService.passid(this.id);
  }
  ngOnInit() {
    this.registerForm2 = this.formBuilder.group({
      adharCard: ['', Validators.required],
      panCard: ['', Validators.required],
      cancelCheque: ['', Validators.required],
    });
  }
  get f() {
    return this.registerForm2.controls;
  }
  register() {
    console.log('Register Function Works1');
    console.log(this.registerForm2.value);
    if (this.registerForm2.valid) {
      this.myservice.submitRegister(this.registerForm2.value).subscribe(
        () => (this.successMessage = 'Registration Successful'),
        () => (this.successMessage = 'Registration Failed')
      );
    }
  }
  onSubmit() {
    const data = this.registerForm2.value;
    const uploadData = new FormData();

    //Appending {data} object to formDate
    Object.keys(data).map((element) => {
      if (!['panCard', 'cancelCheque', 'adharCard'].includes(element)) {
        uploadData.append(element, data[element]);
      }
    });

    //adharCard Append
    const adharCard = this.adharCard.nativeElement.files;
    if (adharCard.length > 0) {
      uploadData.append('adharCard', adharCard[0]);
    }

    //pancard Append
    const panCard = this.panCard.nativeElement.files;
    if (panCard.length > 0) {
      uploadData.append('panCard', panCard[0]);
    }

    //cancelCheque Append
    const cancelCheque = this.cancelCheque.nativeElement.files;
    if (cancelCheque.length > 0) {
      uploadData.append('cancelCheque', cancelCheque[0]);
    }

    this.submitted = true;
    if (this.registerForm2.valid) {
      this.http.post(this.url, uploadData).subscribe((response) => {
        this.router.navigate(['/expertlogin']);
      });
    }
    // console.log(this.registerForm2.valueChanges);
  }
}
