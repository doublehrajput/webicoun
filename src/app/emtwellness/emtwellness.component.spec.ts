import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmtwellnessComponent } from './entwellness.component';

describe('EmtwellnessComponent', () => {
  let component: EmtwellnessComponent;
  let fixture: ComponentFixture<EmtwellnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmtwellnessComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmtwellnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
