import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from './loader.service';
import { API_BASE_URL, SERVER_BASE_URL } from 'src/environments/environment';
import { finalize, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class NewserviceService {
  constructor(private http: HttpClient, private _loader: LoaderService) {}

  _expert = {
    firstName: '',
    lastName: '',
    image: '',
    email: '',
    mobileNumber: '',
    category: '',
    subcategory: '',
    ratings: 0,
    reviews: '',
    products: '',
    description: '',
    _id: '',
  };
  obj:any={};

  
  expert_category

  setexpert(expert) {
    this._expert = expert;
  }

  getexpert(expert) {
    return this._expert;
  }

  getExpertProfile = (id) => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'expert/allexperts/' + id).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  contactexpert = (id) => {
  
  this.obj={"category":this.expert_category,
  "expertId":id}
  //console.log(JSON.stringify(this.obj));

    this._loader.show();

    return this.http
      .post(API_BASE_URL + 'contact-expert',this.obj)
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  allExpert = (id) => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'expert/category/' + id, {
    }).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  raiseQuery = (body) => {
  this.expert_category=body.category;
  //console.log(this.expert_category);
  //  console.log(JSON.stringify(body));
    return this.http.post(API_BASE_URL + 'raise-query', body)
  };

  filterExperts = (category) => {
    this._loader.show();

    return this.http.get(SERVER_BASE_URL + '/api/filter-experts', {
      params: {
        category: category
      }
    }).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };
}
