import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LoginserviceService } from '../loginservice.service';
import { Service2Service } from '../service2.service';
import * as AOS from 'aos';
import { NewserviceService } from '../newservice.service';
import { MessageService } from '../message.service';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  banners: any = [];
  userName;
  id:any;
  expert_url:any;
  experts: any = [];
  constructor(
    private router: Router, private _S2S: Service2Service, private _ARouter: ActivatedRoute, private _ls: LoginserviceService,private _expertService: NewserviceService,
    private _MS: MessageService
    ) {}

  ngOnInit() {
    AOS.init();
    


    this._ls.getLoggedInName.subscribe((name: any) => {
      this.userName = name;
    });

   /* this.expert_url=localStorage.getItem("expert_id");
    console.log("world");
    console.log(this.expert_url);
    if(this.expert_url)
    {
       var isloggedin = localStorage.getItem('JW Token');
        if(isloggedin){
        console.log("hellllo");
            this.router.navigate(['/exprtprofile/'+this.expert_url]);
                      }
        else{
        console.log("llll");
        localStorage.setItem('_routeto', '/exprtprofile/'+ this.expert_url);
      document.getElementById('loginbutton').click();
    } 
    } */
  }

  bookclass(){
   /* var isloggedin = localStorage.getItem('JW Token');

    if(isloggedin){
      this.router.navigate(['/livesession/'])
    }else{
      document.getElementById('loginbutton').click();
    } */
    //this._MS.show('Feature Will be available Soon');
  }
}
