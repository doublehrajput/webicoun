import { Injectable } from '@angular/core';
import {
  Router,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';

@Injectable()
export class ExpertGuard {
  private _role = localStorage.getItem('role');

  constructor(private router: Router) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    this._role = localStorage.getItem('role');
    if (this._role !== 'expert') {
      this.router.navigate(['']);
      return true;
    } else {
      return true;
    }
  }
}
