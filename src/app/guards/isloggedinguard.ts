import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable()
export class Isloggedinguard {
  private _sessionId;

  constructor(private router: Router) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    this._sessionId = localStorage.getItem('JW Token');

    if (this._sessionId !== null) {
      this.router.navigate(['']);
      return true;
    } else {
      return true;
    }
  }
}
