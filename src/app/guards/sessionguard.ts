import { Injectable } from '@angular/core';
import {
  Router,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';

@Injectable()
export class SessionGuard {
  private _sessionId = localStorage.getItem('JW Token');

  constructor(private router: Router) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    this._sessionId = localStorage.getItem('JW Token');

    if (this._sessionId === null) {
      this.router.navigate(['']);
      return true;
    } else {
      return true;
    }
  }
}
