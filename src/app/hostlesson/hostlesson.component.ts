import { Component, OnDestroy, OnInit } from '@angular/core';
import RTCClient from '../utils/RTCClient';
import { LoaderService } from '../loader.service';
import { Service2Service } from '../service2.service';
import { MessageService } from '../message.service';
import { Router } from '@angular/router';
import FirebaseService from '../apis/FirebaseService';

const document: any = window.document;

@Component({
  selector: 'app-hostlesson',
  templateUrl: './hostlesson.component.html',
  styleUrls: ['./hostlesson.component.css'],
})
export class HostLessonComponent implements OnInit, OnDestroy {
  public client;
  public fb;
  public joined = false;
  public date;
  public id;

  public messages = [];
  public child_added;
  private _messageFirstload = false;

  constructor(
    private _loader: LoaderService,
    private _S2S: Service2Service,
    private _MSGS: MessageService,
    private _router: Router
  ) {
    this.client = new RTCClient();
    this.client.createClient();
    this.fb = new FirebaseService();

    this.id = localStorage.getItem('id');
  }

  ngOnInit(): void { }

  ngOnDestroy() {
    this.fb.db
      .ref('webicoun_chats/' + this.date)
      .off('child_added', this.child_added);
    this.leave();
  }

  join = async () => {
    this._loader.show();

    try {
      const agoratoken : any = await this._S2S.getAgoraToken();
      let data = Object.assign({}, agoratoken , {host: true})
     
      this.client
          .join(data)
          .then(() => {
            this._loader.hide();
            this.joinchat();
            this.joined = true;

            var video = document.getElementById('remote-stream');

            video.srcObject = this.client._localStream.stream;
            video.play();

            this.client.publish();
          })
          .catch((error) => {
            console.log(error);

            this._MSGS.show(
              'Permission denied - Streaming could not start'
            );
          });

    } catch (error) {
      
    }
    
  };

  leave = () => {
    if (this.joined) {
      this.client.closeStream();
    }

    this._S2S.closestream().subscribe(() => {
      this._router.navigate(['/']);
    });
  };

  joinchat = () => {
    this.date = new Date().toISOString().substring(0, 10).replace('T', ' ');

    this.child_added = this.fb.db
      .ref('webicoun_chats/' + this.date)
      .on('child_added', (snapshot) => {
        var val = snapshot.val();

        this.scrollWindow();
        this.messages.push(val);
      });

    setTimeout(() => {
      this._messageFirstload = true;
    }, 10000);
  };

  toggleFullscreen = () => {
    const player = document.querySelector('.player');
    const video = document.getElementsByTagName('video')[0];

    if (!document.webkitFullscreenElement) {
      if (video.requestFullScreen) {
        player.requestFullScreen();
      } else if (video.webkitRequestFullScreen) {
        player.webkitRequestFullScreen();
      } else if (video.mozRequestFullScreen) {
        player.mozRequestFullScreen();
      }
    } else {
      document.webkitExitFullscreen();
    }
  };

  play = (status) => {
    if (status) {
      this.client._localStream.unmuteVideo();
      this.client._localStream.unmuteAudio();
    } else {
      this.client._localStream.muteVideo();
      this.client._localStream.muteAudio();
    }
  };

  volumechange = ($event) => {
    if ($event.srcElement.muted) {
      this.client._localStream.muteAudio();
    } else {
      this.client._localStream.unmuteAudio();
    }
  };

  sendMessage = (inpM: any) => {
    const value = inpM.value;

    if (value != '') {
      this.fb.db.ref('webicoun_chats/' + this.date).push({
        content: value,
        timestamp: Date.now(),
        name: localStorage.getItem('userName'),
        id: localStorage.getItem('id'),
      });

      inpM.value = '';
    } else {
      alert('Please Enter A Message');
    }
  };

  scrollWindow = () => {
    setTimeout(() => {
      var element = document.getElementsByClassName('chat-box')[0];
      element.scrollTop = element.scrollHeight + 600;
    }, 500);
  };
}
