import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastWebinarsComponent } from './past-webinars.component';

describe('PastWebinarsComponent', () => {
  let component: PastWebinarsComponent;
  let fixture: ComponentFixture<PastWebinarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastWebinarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastWebinarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
