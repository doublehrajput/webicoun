import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Service2Service } from '../service2.service';

@Component({
  selector: 'app-past-webinars',
  templateUrl: './past-webinars.component.html',
  styleUrls: ['./past-webinars.component.css']
})
export class PastWebinarsComponent implements OnInit {
  webinars: any[] = [];
  visiblewebinars = [];
  page = 1;
  constructor(private _S2S: Service2Service, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this._S2S
      .pastwebinars()
      .subscribe((data: any) => {
        console.log(JSON.stringify(data));
        data.webinars.map((webinar, index) => {

          if(webinar.category === "older"){
            webinar.webiurl = this.getembedurl(webinar.webiurl);
            
            
              this.webinars.push(webinar);
              
            
            
          }
        });        
       
        this.pageChanged({page: 1});
      });
  }

  getembedurl(url2) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match1 = url2.match(regExp);
    
      if(match1)
      {
        return this.sanitizer.bypassSecurityTrustResourceUrl('//www.youtube.com/embed/'+ match1[2]);
      }
      else{
        return this.sanitizer.bypassSecurityTrustResourceUrl(url2);
      }
  }

  pageChanged(event){
    this.page = event.page;

    let s = (this.page - 1 ) * 10;
    let e = s + 10;
    this.visiblewebinars = this.webinars.filter((webinar, index) => {
      return s <= index && index < e;
    })
  }

}
