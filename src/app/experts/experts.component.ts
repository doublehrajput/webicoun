import { Component, OnInit } from '@angular/core';
import { NewserviceService } from '../newservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { getCategoryName } from "../utils";
import { categories } from 'src/environments/environment'


@Component({
  selector: 'app-experts',
  templateUrl: './experts.component.html',
  styleUrls: ['./experts.component.css'],
})
export class ExpertsComponent implements OnInit {
  category;
  experts: any = []; //for searching
  allexperts: any = []; //for searching
  visibleexperts = [];
  page = 1;
  search = '';
  categories= [];
  category_url:any;

  constructor(
    private _expertService: NewserviceService,
    private router: Router,
    private _ARouter: ActivatedRoute,
  ) {

  }

  ngOnInit() {
    this._ARouter.params.subscribe((params) => {
      this.category = getCategoryName(params.category);
      this.category_url=params.category;
      //console.log(this.category_url);
      //this._expertService.raiseQuery({category: params.category}).subscribe();

      this.getExperts(params.category);


    });

    for (const key in categories) {
      this.categories.push({
        id: key,
        text: categories[key]
      });
    }


  }

  getExperts(category){
    this._expertService.allExpert(category).subscribe((data: any) => {
     // console.log(JSON.stringify(data));
      this.experts = data.experts;
      this.allexperts = data.experts;
      //document.getElementById('pagees').click();
      this.pageChanged({page: 1});
    });
  }

  searchfilter($event){
    var search = $event;
    //console.log(JSON.stringify(this.allexperts));
    this.experts = this.allexperts.filter((expert, index) => {
      let name = expert.firstName + ' ' + expert.lastName;
      //console.log(name);
      return !search || name.indexOf(search) >= 0;
    });
    //document.getElementById('pagees').click();
    this.pageChanged({page: 1});
  }

  pageChanged(event){
    this.page = event.page;
    //console.log('hello'+this.page);

    let s = (this.page - 1 ) * 10;
    let e = s + 10;
    //console.log(e);
    this.visibleexperts = this.experts.filter((expert, index) => {
      return s <= index && index < e;
    })
  }

  changecategory(id){
   this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
  this.router.navigate(['/experts/' + id]));
  }

  viewProfile(id)
  {
    //console.log(id);
    this._expertService.raiseQuery({category: this.category_url,expertId:id}).subscribe();
  
    this.router.navigate(['/exprtprofile/' + id]);
  }

}
