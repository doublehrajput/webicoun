import { Component, OnInit } from '@angular/core';
import { BlogserviceService } from '../blogservice.service';
import { Service2Service } from 'src/app/service2.service';
import {chunk} from '../utils/index';
import { ActivatedRoute, Router } from '@angular/router';
import { categories } from 'src/environments/environment';
import { getCategoryName } from "../utils";



@Component({
  selector: 'app-descriptionblogs',
  templateUrl: './descriptionblogs.component.html',
  styleUrls: ['./descriptionblogs.component.css'],
})
export class DescriptionblogsComponent implements OnInit {
  url = 'http://webicounindia.herokuapp.com/blogs;';
  tags: any = [];
  id: any;

  //blog = { description: '', title: '', image: '',tags:'' };

  constructor(private _blogService: BlogserviceService,
    private router: Router, private _S2S: Service2Service, private _ARouter: ActivatedRoute) {}

  title: any;
  tagees:any=[];
  img: any;
  blogs: any = [];
  description: any;
  tag: any = [];
  author: any;
  ngOnInit() {



    this._ARouter.params.subscribe((params) => {

      this.id = params['id'];

      this._S2S
        .blogdescription(this.id)
        .subscribe((data: any) => {
         // console.log(JSON.stringify(data));
          this.blogs = data.blog;
          this.title = this.blogs[0].title;
          this.description = this.blogs[0].description;
          this.img = this.blogs[0].image;
          this.tag = this.blogs[0].tags;
          this.author = this.blogs[0].author;
         
         




        });

    });
  }

  tagClick(title)
{     
      console.log(title);
      this.router.navigate(['/tag/' + title]);

}

}