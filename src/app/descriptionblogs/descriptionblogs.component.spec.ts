import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionblogsComponent } from './descriptionblogs.component';

describe('DescriptionblogsComponent', () => {
  let component: DescriptionblogsComponent;
  let fixture: ComponentFixture<DescriptionblogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescriptionblogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionblogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
