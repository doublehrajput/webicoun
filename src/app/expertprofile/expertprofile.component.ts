import { Component, OnInit } from '@angular/core';
import { NewserviceService } from '../newservice.service';
import { Service2Service } from 'src/app/service2.service';
import { MessageService } from 'src/app/message.service';
import { RazorpayService } from '../apis/razorpay';
import { RAZORPAY_OPTIONS } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';

declare let Razorpay: any;


@Component({
  selector: 'app-expertprofile',
  templateUrl: './expertprofile.component.html',
  styleUrls: ['./expertprofile.component.css'],
})
export class ExpertprofileComponent implements OnInit {
  expert = {
    firstName: '',
    lastName: '',
    image: '',
    email: '',
    mobileNumber: '',
    category: '',
    subcategory: '',
    ratings: 0,
    reviews: '',
    products: [],
    description: '',
    _id: '',
  };
  constructor(
    private _expertService: NewserviceService,
    // public dialog: MatDialog,
    public _S2S: Service2Service,
    private _razor: RazorpayService,
    private _MS: MessageService,
    private _ARouter: ActivatedRoute,
    private _router: Router
  ) {
    this._ARouter.params.subscribe((params) => {

      this._S2S.expertreviews(params.id).subscribe((res) => {
        console.log(res);

      })

      this._expertService.getExpertProfile(params.id).subscribe(
        (res: any) => {
          if (res.hasOwnProperty('experts')) {
            this.expert = res.experts;
          } else {
            this._router.navigate(['/experts']);
          }
        },
        () => {
          this._router.navigate(['/experts']);
        }
      );
    });
  }


  ngOnInit() {



  }

  pay(product: any) {
    product.expertid = this.expert._id;
    this._S2S.expertbuypackage(product).subscribe((response: any) => {
      let op = this.setRazorPay({
        amount: response.amount,
        currency: response.currency,
        order_id: response.id,
        handler: this.razorPaySuccessHandler.bind(this),
      });

      let razorpay = new Razorpay(op);
      razorpay.open();
    });
  }

  setRazorPay = (options) => {
    return Object.assign(
      {},
      RAZORPAY_OPTIONS,
      {
        prefill: {
          name: this._S2S.name,
          email: this._S2S.email,
          contact: this._S2S.contact,
          method: '',
        },
      },
      options
    );
  };

  public razorPaySuccessHandler(response) {
    response.expertId = this.expert._id;

    this._razor.completePackagepayment(response).subscribe(
      (success) => {
        this._MS.show('Payment Success');
      },
      (error) => {
        this._MS.show('Payment Failed');
      }
    );
  }

  contact = () => {
    this._S2S.contactexpert(this.expert._id).subscribe((response: any) => {
      this._MS.show(response.message);
    });
  };

  rate = () => {
    this._S2S.rateexpert(this.expert._id, {
      text: 'Hi',
      rating: 5
    }).subscribe((response: any) => {
      this._MS.show(response.message);
    });
  }
}
