import { Component, OnInit } from '@angular/core';
import { BlogserviceService } from '../blogservice.service';
import { Service2Service } from 'src/app/service2.service';
import {chunk} from '../utils/index';
import { ActivatedRoute, Router } from '@angular/router';
import { categories } from 'src/environments/environment';
import { getCategoryName } from "../utils";


@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

title:any;
blogs:any=[];

  constructor(private _blogService: BlogserviceService,
    private router: Router, private _S2S: Service2Service, private _ARouter: ActivatedRoute) {}



  ngOnInit(): void {


    this._ARouter.params.subscribe((params) => {

      this.title = params['title'];


      this._S2S
        .tagclick(this.title)
        .subscribe((data: any) => {
          console.log(JSON.stringify(data));
          this.blogs=data.blogs;
         });

    });
  }

read(id) {
   
    this.router.navigate(['/descriptionblogs/'+id]);
  }

}
