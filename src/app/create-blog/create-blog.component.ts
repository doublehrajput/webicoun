import { Component, OnInit } from '@angular/core';

import {FormsModule} from '@angular/forms'
@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {
  public options: Object = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: false
  }
  constructor() { }

  ngOnInit(): void {
  }

}
