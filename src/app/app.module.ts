import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgImageSliderModule } from 'ng-image-slider';
import { ToastrModule } from 'ngx-toastr';
import {MatTooltipModule} from '@angular/material/tooltip';
import { FlexLayoutModule } from "@angular/flex-layout";
import { RegisterServiceService } from './register-service.service';
import { Service2Service } from './service2.service';
import { LoginserviceService } from './loginservice.service';
import { BlogserviceService } from './blogservice.service';
import { NewserviceService } from './newservice.service';
import { LoaderService } from './loader.service';
import { ExpertsignupsService } from './expertsignups.service';
import { MessageService } from './message.service';
import { SlideshowModule } from 'ng-simple-slideshow';
import {NgxPaginationModule} from 'ngx-pagination';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HeaderLoginComponent } from './header/headerlogin/headerlogin.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AuthenticationInterceptor } from './utils';
import { RazorpayService } from './apis';
import { CroppieModule } from 'angular-croppie-module';

import {
  SessionGuard,
  Isloggedinguard,
  UserGuard,
  ExpertGuard,
} from './guards';
import { AppRoutingModule, RoutingComponent } from './app-routing.module';
import { PastWebinarsComponent } from './past-webinars/past-webinars.component';
import { ExpertprofileComponent } from './expertprofile/expertprofile.component';
import { ExprtprofileComponent } from './exprtprofile/exprtprofile.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { WebinarloginComponent } from './webinarss/webinarlogin/webinarlogin.component';
import { BlogCategoryComponent } from './blog-category/blog-category.component';
import { TagComponent } from './tag/tag.component';
//import { UploadProfileComponent } from './expertdashboard/upload-profile/upload-profile.component';


@NgModule({
  declarations: [
    AppComponent,
    RoutingComponent,
    HeaderComponent,
    HeaderLoginComponent,
    FooterComponent,
    PastWebinarsComponent,
    ExpertprofileComponent,
    ExprtprofileComponent,
    CreateBlogComponent,
    WebinarloginComponent,
    BlogCategoryComponent,
    TagComponent
   // UploadProfileComponent

  ],
  imports: [
    BrowserModule,
    NgImageSliderModule,
    NgxPaginationModule,
    MatDialogModule,
    MatTooltipModule,
    PaginationModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    HttpClientModule,
    RouterModule,
    ToastrModule.forRoot(),
    CommonModule,
    SlideshowModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    ImageCropperModule,
    CroppieModule 
  ],
  providers: [
    NewserviceService,
    RegisterServiceService,
    Service2Service,
    LoginserviceService,
    ExpertsignupsService,

    BlogserviceService,
    LoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    },
    RazorpayService,
    SessionGuard,
    Isloggedinguard,
    MessageService,
    UserGuard,
    ExpertGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
