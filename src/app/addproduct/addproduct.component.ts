import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Service2Service } from 'src/app/service2.service';
import { MessageService } from 'src/app/message.service';
import { API_BASE_URL } from 'src/environments/environment';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css'],
})
export class AddproductComponent implements OnInit {
  url = API_BASE_URL + 'expert/add-session';

  registerForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private _S2S: Service2Service,
    private _MS: MessageService,
    public dialogRef: MatDialogRef<any>
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      title: ['', Validators.required],
      platform: ['', Validators.required],
      category: ['', Validators.required],
      sessions: ['', Validators.required],
      price: ['', Validators.required],
      description: ['',[ Validators.required]],
    });
  }

  get f() {
    return this.registerForm.controls;
  }
  onSubmit(data) {
    this.submitted = true;

    this._S2S.addsession(data).subscribe(
      (success: any) => {
        this._MS.show(success.message);
        this.dialogRef.close();
      },
      (error: any) => {
        this._MS.show(error?.error?.error);
      }
    );
  }
}
