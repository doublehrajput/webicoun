import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocwellnessComponent } from './socwellness.component';

describe('SocwellnessComponent', () => {
  let component: SocwellnessComponent;
  let fixture: ComponentFixture<SocwellnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocwellnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocwellnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
