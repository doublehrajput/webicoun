import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginserviceService } from '../loginservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-expert-sign-in',
  templateUrl: './expert-sign-in.component.html',
  styleUrls: ['./expert-sign-in.component.css'],
})
export class ExpertSignInComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  password = "";

  show = false;

  emailnotverified = false;

  constructor(
    private formBuilder: FormBuilder,
    private _LS: LoginserviceService,
    private router: Router
  ) { }

  ngOnInit() {
    this.password = 'password';
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern]],
      otp: ['', []],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  validateotp () {
    this._LS.validateotp(this.registerForm.value).subscribe(
      (res) => {
        this._LS.logmeexpert(res);
        this.router.navigate(['/expertdashboard']);
      },
      (error) => {
        
      }
    );
  }

  resendotp(){
    this._LS.resendotp(this.registerForm.value).subscribe(
      () => {
      },
      (error) => {
        
      }
    );
  }

  onSubmit() {
    this.submitted = true;

    this._LS.expertlogin(this.registerForm.value).subscribe(
      (res) => {

        if(res === false){
          this.emailnotverified = true;
        }else{

          this.router.navigate(['/expertdashboard']);
        }
        
      },
      (error) => {
        this.registerForm.controls.email.setErrors({
          email: error?.error?.error,
        });
      }
    );
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }



  onClickk() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }
}
