import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertSignInComponent } from './expert-sign-in.component';

describe('ExpertSignInComponent', () => {
  let component: ExpertSignInComponent;
  let fixture: ComponentFixture<ExpertSignInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertSignInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertSignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
