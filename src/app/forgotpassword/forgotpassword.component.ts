import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginserviceService } from '../loginservice.service';
import { MessageService } from '../message.service';

@Component({

  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
})
export class ForgotPasswordComponent {
  form: FormGroup;
  show = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private _LS : LoginserviceService,
    private _MS: MessageService

  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  get f() {
    return this.form.controls;
  }


  forgotpassword = () => {
    this._LS.forgotpassword(this.form.value).subscribe(
        (response: any) => {
            this._MS.show(response.message);
        }
    )
  }

}
