import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoaderService } from './loader.service';
import { finalize, map } from 'rxjs/operators';
import { API_BASE_URL, SERVER_BASE_URL } from 'src/environments/environment';
import { Observable, forkJoin } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class Service2Service {
  constructor(private http: HttpClient, private _loader: LoaderService) { }

  name = '';
  email = '';
  contact = '';
  id = '5f854e33a484380017d8630d';

  setPay = (data) => {
    this.name = data.name;
    this.email = data.email;
    this.contact = data.contact;
    this.id = data.id;
  };

  booksession = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'booksession', data).pipe(
      map((r) => {
        console.log(r);
        return r;
      }),
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  banners = () => {
    return this.http.get(API_BASE_URL + 'banners');
  };

  livelessons = () => {
    this._loader.show();

    return forkJoin([
      this.http.get(API_BASE_URL + 'allliveclasses'),
      this.http.get(API_BASE_URL + 'allpastclasses'),
      this.http.get(API_BASE_URL + 'users/me'),
      this.http.get(API_BASE_URL + 'all-courses'),
    ]).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };



  livelessonjoin = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'liveclasses').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  upcomingWebinars = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'upcomingwebinars').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


 blogdescription= (id) => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'blogs/'+id).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  purchaseplan = (pack) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'liveclasses/' + pack, {}).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  webinarPack = (id) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'subscribe-webinar/payment',{webinar:id}).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  hoststeam = () => {
    return this.http.post(API_BASE_URL + 'admin/startliveclass', {});
  };

  uploadfile = (data) => {
    this._loader.show();

    return this.http.put(API_BASE_URL + 'users/userPic', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  uploadprofile = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/upload-profilePic', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  bloglist = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'blogcat').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


 tagclick = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'blog-tag',{tag:data}).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  blogbycategory = (categoryname) => {
    this._loader.show();
   

    return this.http.post(API_BASE_URL + 'blog-category',{category:categoryname}).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  editprofile = (data) => {
    this._loader.show();

    return this.http.put(API_BASE_URL + 'users/edit-profile', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  edit_expert_profile = (data) => {
    this._loader.show();

    return this.http.patch(API_BASE_URL + 'expert/update-info', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  closestream = () => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'admin/endliveclass', {}).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  addsession = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/add-session', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  deletesession = (id) => {
    this._loader.show();

    return this.http.delete(API_BASE_URL + 'expert/delete-session/' + id).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  expertbuypackage = (data) => {
    this._loader.show();
    var d = {
      productid: data._id,
      expertid: data.expertid,
    };

    return this.http.post(API_BASE_URL + 'expertpackage/payment', d).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  me = () => {
    return this.http.get(SERVER_BASE_URL + '/api/me');
  };

  expert = () => {
    return this.http.get(SERVER_BASE_URL + '/api/expert');
  };

  contactus = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'contactus', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  savenotificationpreferences = (data) => {
    this._loader.show();

    return this.http
      .post(SERVER_BASE_URL + '/api/update-notification', data)
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  enquiry = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'raise-query', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  readnotification = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/read-notification', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  expertplan = (pack) => {
    this._loader.show();

    return this.http
      .post(API_BASE_URL + 'expert-premium/payment', { category: pack })
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  contactexpert = (data) => {
    this._loader.show();

    return this.http
      .post('https://webicoun-backend-hajkn.run-ap-south1.goorm.io/contact-expert', data)
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  uploadexpertbioVideo = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/upload-bioVideo', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  uploadexpertpostdata = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/upload-post', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  rateexpert = (id, data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/' + id + '/add-review', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  expertreviews = (id) => {
    return this.http.get(API_BASE_URL + 'expert/' + id + '/reviews');
  };

  pastwebinars = () => {
    //this._loader.show();

    return this.http.get(API_BASE_URL + 'webinars').pipe(
      finalize(() => {
       // this._loader.hide();
      })
    );
  }

  pastclasses = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'allpastclasses').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  getAgoraToken = () => {
    return this.http.get(API_BASE_URL + 'startLiveCall').toPromise();
  }

  getExpertMobileOtp = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/verify-phnnum' , data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  verifyExpertMobile = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/verify-smsOTP' , data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  bookfreeclass = () => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'bookfreeclasses', {} ).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  addmorecategory = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/add-category', data ).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  addnewfeed = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/addnewfeed', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  uploadpost = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/upload-post', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  addlanguage = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/add-language', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  addqualification = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/add-education', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  gallery = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'expert/myGallery').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  uploaddocs = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/register-expert2', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  editKycdocs= () => {
    this._loader.show();
    var data={};

    return this.http.post(API_BASE_URL + 'expert/edit-kyc',data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  deletepost = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/delete-post', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };



  deletelang = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/delete-language', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  deleteeducation = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/delete-education', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  deletecategory = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL+ 'expert/delete-category', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  deletevideo = () => {
    this._loader.show();
  var  data={a:'fg'};

    return this.http.post(API_BASE_URL+'expert/delete-bioVideo',data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  requesteditprofile = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'expert/request-editprofile').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  requesteditwebiner = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'expert/request-webinar').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  startcourse = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'start-course', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }


  getcourse = (data) => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'get-course/' +  data.courseId).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }


  playvideo = (courseId, videoId) => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'get-courseVideo/' + courseId +'/' + videoId,).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }


  getcertificate = (courseId) => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'get-certificate/' + courseId , {
      responseType: 'blob' as 'json'
    }).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

}
