import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentdashnavComponent } from './studentdashnav.component';

describe('StudentdashnavComponent', () => {
  let component: StudentdashnavComponent;
  let fixture: ComponentFixture<StudentdashnavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentdashnavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentdashnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
