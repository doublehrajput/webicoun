import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Service2Service } from '../service2.service';
import { MessageService } from '../message.service';
import { RazorpayService } from '../apis';
import { Router } from '@angular/router';

import {RAZORPAY_OPTIONS} from 'src/environments/environment'

declare let Razorpay: any;

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {

  constructor(private cd: ChangeDetectorRef, 
    private _S2S: Service2Service, private _MS: MessageService,
    private _razor: RazorpayService,
    private _router: Router,
    ) { }

  ngOnInit(): void {
  }


  setRazorPay = (options) => {
    return Object.assign({}, RAZORPAY_OPTIONS, options);
  };

  public pay(pack: string) {
    this._S2S.purchaseplan(pack).subscribe(
      (response: any) => {
        let op = this.setRazorPay({
          amount: response.amount,
          currency: response.currency,
          order_id: response.id,
          handler: this.razorPaySuccessHandler.bind(this),
        });

        let razorpay = new Razorpay(op);
        razorpay.open();
      },
      () => {
        this._MS.show('Internal Server Error');
      }
    );
  }

  public razorPaySuccessHandler(response) {
    this._razor.purchaseplan(response).subscribe(
      (response) => {
        this._MS.show('Payment Success');
        this._router.navigate(['/livesession']);
      },
      (error) => {
        this._MS.show('Payment Failed');
      }
    );
  }

}
