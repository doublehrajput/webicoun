import { Component, OnInit } from '@angular/core';
import { BlogserviceService } from '../blogservice.service';

import { Service2Service } from 'src/app/service2.service';
import {chunk} from '../utils/index';
import { ActivatedRoute, Router } from '@angular/router';
import { categories } from 'src/environments/environment';
import { getCategoryName } from "../utils";

@Component({
  selector: 'app-blog-category',
  templateUrl: './blog-category.component.html',
  styleUrls: ['./blog-category.component.css']
})
export class BlogCategoryComponent implements OnInit {
  constructor(private _service: BlogserviceService, private router: Router,private _S2S: Service2Service,private _ARouter: ActivatedRoute) {}

  blogs1: any = [];

  image = [];
  category:any;
  blogs:any=[];
  blogee:any=[];
  bloglist:any=[];
  noData=false;
  

  ngOnInit() {

     this._S2S
    .bloglist()
    .subscribe((data: any) => {
    //console.log("n");
      
      this.blogs=data.blogcat;
      //this.bloglist=data.blogs;
      this.blogee = chunk(this.blogs, 8);
   
      


    });

 
     this._ARouter.params.subscribe((params) => {

      this.category = params['category']
      //console.log(this.category);
         
  this._S2S
    .blogbycategory(this.category)
    .subscribe((data: any) => {
     //console.log(JSON.stringify(data));
  
      this.bloglist=data.blogs;
     if(!this.bloglist.length)
      {
      this.noData=true;
      }
      else{
        this.noData=false;
      }
     


    });


    });


    this._service
      .list()
      .subscribe((response: any) => (this.blogs1 = response.blogs));
  }

  read(id) {
   // this._service.setBlog(blog);
    this.router.navigate(['/descriptionblogs/'+id]);
  }

  
 clickImg(category)

 {


   this.router.navigate(['/blog-category/' + category]);
   


   

  }
}

