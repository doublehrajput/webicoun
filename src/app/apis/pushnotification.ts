import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_BASE_URL } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PushNotificationService {
  applicationServerKey =
    'BHU9c2J3GfH_87iC96S8uqXC5fqqc_PPbXO6aPA6VXqiZttW0Dz8f8y_pj9d1cOXTO4cy5_rvMnnWn39SSZe_YY';

  constructor(private http: HttpClient) {}

  urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }

  checkNotificationPermission() {
    return new Promise((resolve, reject) => {
      if (Notification.permission === 'denied') {
        return reject(new Error('Push messages are blocked.'));
      }

      if (Notification.permission === 'granted') {
        return resolve('');
      }

      if (Notification.permission === 'default') {
        return Notification.requestPermission().then((result) => {
          if (result !== 'granted') {
            reject(new Error('Bad permission result'));
          } else {
            resolve('');
          }
        });
      }

      return reject(new Error('Unknown permission'));
    });
  }

  push_subscribe() {
    return this.checkNotificationPermission()
      .then(() => navigator.serviceWorker.ready)
      .then((serviceWorkerRegistration) =>
        serviceWorkerRegistration.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: this.urlBase64ToUint8Array(
            this.applicationServerKey
          ),
        })
      )
      .then((subscription) => {
        return this.push_sendSubscriptionToServer(subscription);
      });
  }

  push_updateSubscription() {
    //Notification Grant Logic
    if (
      !('serviceWorker' in navigator) ||
      !('PushManager' in window) ||
      !('showNotification' in ServiceWorkerRegistration.prototype) ||
      Notification.permission === 'denied'
    ) {
      console.warn('Service workers are not supported by this browser');
      return;
    }

    navigator.serviceWorker.register('sw.js').then(() => {
      navigator.serviceWorker.ready
        .then((serviceWorkerRegistration) =>
          serviceWorkerRegistration.pushManager.getSubscription()
        )
        .then((subscription) => {
          if (!subscription) {
            return this.push_subscribe();
          }

          return this.push_sendSubscriptionToServer(subscription);
        })
        .then((subscription) => subscription) // Set your UI to show they have subscribed for push messages
        .catch((e) => {
          console.error('Error when updating the subscription', e);
        });
    });
  }

  push_sendSubscriptionToServer(subscription) {
    const key = subscription.getKey('p256dh');
    const token = subscription.getKey('auth');
    const contentEncoding = (PushManager.supportedContentEncodings || [
      'aesgcm',
    ])[0];

    return this.registerDevice({
      endpoint: subscription.endpoint,
      publicKey: key
        ? btoa(String.fromCharCode.apply(null, new Uint8Array(key)))
        : null,
      authToken: token
        ? btoa(String.fromCharCode.apply(null, new Uint8Array(token)))
        : null,
      contentEncoding,
      userId: localStorage.getItem('id'),
    }).subscribe((response) => {});
  }

  registerDevice = (data) => {
    return this.http.post(SERVER_BASE_URL + '/api/add-subscription', data);
  };
}