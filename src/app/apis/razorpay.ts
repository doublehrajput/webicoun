import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from '../loader.service';
import { finalize } from 'rxjs/operators';
import { API_BASE_URL, SERVER_BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RazorpayService {
  constructor(private http: HttpClient, private _loader: LoaderService) {}

  generateOrderId = (id, data) => {
    this._loader.show();

    return this.http
      .post(API_BASE_URL + 'book-csession/razorpay/' + id, data)
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  completePayment = (id, data) => {
    this._loader.show();

    return this.http
      .post(API_BASE_URL + 'book-csession/verify/' + id, data)
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  purchaseplan = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'liveclasses/verify', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  webinarPack= (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'subscribe-webinar/verify', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  completePackagepayment = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expertpackage/verify', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  acceptPayment = (data) => {
    this._loader.show();

    return this.http.post(SERVER_BASE_URL + 'api/verify-payment', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  purchaseexpertplan = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert-premium/verify', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };
}
