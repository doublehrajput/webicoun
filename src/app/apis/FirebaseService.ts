import * as firebase from 'firebase/app';
import 'firebase/database';


import {firebaseConfig} from 'src/environments/environment';

firebase.initializeApp(firebaseConfig);

export default class FirebaseService {

    public db;

  constructor() {


    this.db = firebase.database();

  }


}
