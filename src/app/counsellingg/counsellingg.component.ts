import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Service2Service } from '../service2.service';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../must-match/must-match.validator';

@Component({
  selector: 'app-counsellingg',
  templateUrl: './counsellingg.component.html',
  styleUrls: ['./counsellingg.component.css'],
})
export class CounsellinggComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private S2S: Service2Service
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      cat: ['', Validators.required],
      name: ['', Validators.required],
      number: ['', [Validators.required, Validators.minLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      ask: [''],
      dob: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }
  onSubmit(data) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.valid) {
      this.submitted = true;
      this.S2S.booksession(data).subscribe(() => {
        this.router.navigate(['/booksession2']);
      });
    }
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
