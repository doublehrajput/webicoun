import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounsellinggComponent } from './counsellingg.component';

describe('CounsellinggComponent', () => {
  let component: CounsellinggComponent;
  let fixture: ComponentFixture<CounsellinggComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounsellinggComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounsellinggComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
