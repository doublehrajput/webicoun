import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from './loader.service';
import { finalize } from 'rxjs/operators';
import { API_BASE_URL } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ExpertsignupsService {
  constructor(private http: HttpClient, private _loader: LoaderService) {}

  id2 = {
    id: '',
  };
  passid(id) {
    this.id2 = id;
  }

  next = (uploadData) => {
    this._loader.show();

    return this.http
      .post(API_BASE_URL + 'expert/register-expert1', uploadData)
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  register = (uploadData, id) => {
    this._loader.show();

    return this.http
      .post(API_BASE_URL + 'expert/register-expert2/' + id, uploadData)
      .pipe(
        finalize(() => {
          this._loader.hide();
        })
      );
  };

  forgotpassword = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/forgotpassword', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  resetpassword = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/passwordreset', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  validateuser = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/verify-emailOTP' , data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }
}
