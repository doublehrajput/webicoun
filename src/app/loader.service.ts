import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  element;

  constructor() {
    this.element = document.getElementById('loader-cover');
  }

  show = () => {
    this.element.style.display = 'block';
  };

  hide = () => {
    this.element.style.display = 'none';
  };
}
