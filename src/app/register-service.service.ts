import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { LoaderService } from './loader.service';
import { API_BASE_URL } from 'src/environments/environment';
import { finalize, map } from 'rxjs/operators';

@Injectable()
export class RegisterServiceService {

  constructor(private http: HttpClient, private _loader: LoaderService) {}

  submitRegister(body: any) {
    return this.http.post('https://webicounindia.herokuapp.com/signup', body,
      {
        observe: 'body'
      });
  }
  login(body: any) {
    return this.http.post('https://webicounindia.herokuapp.com/signin', body,
      {
        observe: 'body'
      });
  }

  signup = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'signup' , data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  validateuser = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'validateuser' , data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }


}
