import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Booksession2Component } from './booksession2.component';

describe('Booksession2Component', () => {
  let component: Booksession2Component;
  let fixture: ComponentFixture<Booksession2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Booksession2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Booksession2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
