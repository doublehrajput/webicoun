import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Service2Service } from '../service2.service';
import { RazorpayService } from '../apis/razorpay';
import {RAZORPAY_OPTIONS} from 'src/environments/environment'

declare let Razorpay: any;

@Component({
  selector: 'app-booksession2',
  templateUrl: './booksession2.component.html',
  styleUrls: ['./booksession2.component.css'],
})
export class Booksession2Component implements OnInit {
  constructor(private _razor: RazorpayService, private _S2S: Service2Service) {}

  ngOnInit(): void {}

  setRazorPay = (options) => {
    return Object.assign(
      {},
      RAZORPAY_OPTIONS,
      {
        prefill: {
          name: this._S2S.name,
          email: this._S2S.email,
          contact: this._S2S.contact,
          method: '',
        },
      },
      options
    );
  };

  public pay(media: string) {
    this._razor
      .generateOrderId(this._S2S.id, { media })
      .subscribe((response: any) => {
        let op = this.setRazorPay({
          amount: response.amount,
          currency: response.currency,
          order_id: response.id,
          handler: this.razorPaySuccessHandler.bind(this),
        });

        let razorpay = new Razorpay(op);
        razorpay.open();
      });
  }

  public razorPaySuccessHandler(response) {
    this._razor.completePayment(this._S2S.id, response).subscribe(
      (success) => {
        alert('Payment Success');
      },
      (error) => {
        alert('Payment Error');
      }
    );
  }
}
