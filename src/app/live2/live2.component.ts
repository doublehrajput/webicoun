import { Component, OnInit } from '@angular/core';
import { Service2Service } from '../service2.service';

import {RAZORPAY_OPTIONS} from 'src/environments/environment'

declare let Razorpay: any;

@Component({
  selector: 'app-live2',
  templateUrl: './live2.component.html',
  styleUrls: ['./live2.component.css']
})
export class Live2Component implements OnInit {

  constructor(private _S2S: Service2Service) { }

  ngOnInit(): void {
  }


  setRazorPay = (options) => {
    return Object.assign(
      {},
      RAZORPAY_OPTIONS,
      {
        prefill: {
          name: this._S2S.name,
          email: this._S2S.email,
          contact: this._S2S.contact,
          method: '',
        },
      },
      options
    );
  };

  public pay(amount: any) {
    let op = this.setRazorPay({
      amount: amount + '00',
      handler: this.razorPaySuccessHandler.bind(this),
    });

    let razorpay = new Razorpay(op);
    razorpay.open();
  }

  public razorPaySuccessHandler(response) {
    console.log(response);
    alert('Payment Success ' + response.razorpay_payment_id);
    // this.cd.detectChanges()
  }

}
