import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginserviceService } from '../loginservice.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  userName: string = '';
  role: string = '';

  constructor(private _ls: LoginserviceService, private router: Router) {
    this._ls.getLoggedInName.subscribe((name: any) => {
      this.userName = name;
      this.role = localStorage.getItem('role');
    });
  }


  ngOnInit(): void {
  }



}
