import { Component, OnInit } from '@angular/core';
import { RegisterServiceService } from '../register-service.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Service2Service } from '../service2.service';
import { MessageService } from '../message.service';
import { SERVER_BASE_URL } from 'src/environments/environment';
import { MustMatch } from '../must-match/must-match.validator';

@Component({
  selector: 'app-studentdashboard',
  templateUrl: './studentdashboard.component.html',
  styleUrls: ['./studentdashboard.component.css'],
})
export class StudentdashboardComponent {
  editprofie = false;

  profile = {
    fullName: '',
    email: '',
    image: 'https://i.postimg.cc/RFMM0wW7/logo.png',
    subscription: {
      category: '',
      paymentDetails: {
        paymentId: null,
      },
      doj: '',
      doe: '',
    },
    age: '',
    gender: '',
    description: '',
    is_cronofy_subscribed: 0,
    cronofy_url: '#',
    _id: false,
  };
  // username = '';
  profileForm: FormGroup;
  passwordForm: FormGroup;
  url = SERVER_BASE_URL + '/api/me';

  submitted = false;
  isSubscribed = false;

  constructor(
    private myservice: RegisterServiceService,
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private _S2S: Service2Service,
    private _MSGS: MessageService
  ) {}
  ngOnInit() {

    this.profileForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      mobileNumber: ['', Validators.required],
      description: ['', Validators.required],
      age: ['', Validators.required],
      gender: ['', Validators.required],
      image: ['', Validators.required],
    });

    this.passwordForm = this.formBuilder.group(
      {
        oldPassword: ['', Validators.required],
        newPassword: ['', Validators.required],
        newPassword2: ['', Validators.required],
      },
      {
        validator: MustMatch('newPassword', 'newPassword2'),
      }
    );

    // console.log(this.profile.subscription.doj)
    this.getProfile();
  }

  pastLiveClasses = [];
  upcomingClasses = [];

  chunk = (arr, size) =>
    Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
        arr.slice(i * size, i * size + size)
  );

  getProfile = () => {
    let d = (new Date).getDay() - 1;

    this._S2S.livelessons().subscribe((response: any) => {
      
      const {liveclasses} = response[0];
      const {pastclasses} = response[1];
      this.profile = response[2];

      this.isSubscribed = this.profile.subscription?.paymentDetails?.paymentId;

      for (const control in this.profileForm.controls) {
        let v = this.profile[control];
        this.profileForm.controls[control].setValue(v);
      }

      this.upcomingClasses = this.chunk(liveclasses.slice(d + 1, 5).concat(liveclasses.slice(0, d+1 )), 3);
      this.pastLiveClasses = this.chunk(pastclasses, 3).slice(0,2);

    });

    // console.log(this.profile.subscription)
  };

  get f() {
    return this.profileForm.controls;
  }

  onSubmit(data) {
    // console.log('jrgnjrnfkj');
    // if (this.profileForm.valid) {
    //   this.submitted = true;
    // }
  }

  onpasswordchange(data) {
    if (this.passwordForm.valid) {
      this.submitted = true;
    }
    // console.log(data);
  }

  get f1() {
    return this.passwordForm.controls;
  }

  changeAvatar = (file) => {
    const uploadData = new FormData();
    uploadData.append('image', file[0]);

    this._S2S.uploadfile(uploadData).subscribe((response: any) => {
      this._MSGS.show(response.message);
      this.profile.image = response.image;
    });
  };

  editprofile = () => {
    this._S2S.editprofile(this.profileForm.value).subscribe((response: any) => {
      this._MSGS.show(response.message);
      this.editprofie = false;

      this.getProfile();
    });
  };
}
