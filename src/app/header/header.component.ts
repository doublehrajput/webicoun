import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { LoginserviceService } from '../loginservice.service';
import { MessageService } from '../message.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { categories } from 'src/environments/environment'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  userName: string = '';
  role: string = '';
  selectedCategory;
  experts: any = [];

  modalRef: BsModalRef;
  categories = [];
  constructor(
    private _ls: LoginserviceService, private router: Router,private modalService: BsModalService,
    private _MS: MessageService
    ) {

    document.addEventListener("click", function () {
      let navbarSupportedContent = document.getElementById('navbarSupportedContent');
      let classes = navbarSupportedContent.classList.value;

      if (classes.includes('show')) {
        var togglemenu = document.getElementById('togglemenu');
        togglemenu.click()
      }
    });

    this._ls.getLoggedInName.subscribe((name: any) => {
      this.userName = name;
      this.role = localStorage.getItem('role');
    });
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  chunk = (arr, size) =>
    Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
        arr.slice(i * size, i * size + size)
  );

  ngOnInit(){
    let cat = []
    for (const key in categories) {
      cat.push({
        id: key,
        text: categories[key]
      });
    }

    this.categories = this.chunk(cat, 5);

  }

  logout = () => {
    this._ls.logoutservie().subscribe(() => {
      this._ls.logout();
      this.router.navigate(['']);
    });
  };


  changecategory(id){

    var isloggedin = localStorage.getItem('JW Token');

    if(isloggedin){
       console.log("hello");
       this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
       this.router.navigate(['/experts/' + id]));
    }else{
      localStorage.setItem('_routeto', '/experts/'+ id);
      document.getElementById('loginbutton').click();
    }

  }

  bookclass(){
    /*var isloggedin = localStorage.getItem('JW Token');

    if(isloggedin){
      this.router.navigate(['/livesession/'])
    }else{
      document.getElementById('loginbutton').click();
    } */
    this._MS.show('Feature Will be available Soon');
  }
}
