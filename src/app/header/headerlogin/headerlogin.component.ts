import { Component, OnInit, TemplateRef , ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { LoginserviceService } from '../../loginservice.service';
import { MessageService } from '../../message.service';


@Component({
  selector: 'app-header-login',
  templateUrl: './headerlogin.component.html',
  styleUrls: ['./headerlogin.component.css'],
})
export class HeaderLoginComponent implements OnInit {

  logintype = '';

  form: FormGroup;
  signupform: FormGroup;

  modalRef: BsModalRef;
  userId: false;
  token: false;
  user: false;
  @ViewChild('signuptemplate') signuptemplate;

  passwordtype = 'password';
  showeye = true;


  constructor(
    private _LS: LoginserviceService,
    private router: Router,private modalService: BsModalService,private formBuilder: FormBuilder,
    private _MS: MessageService
    ) {
    this.form = this.formBuilder.group({
      password: ['', []],
      mobile: ['', [Validators.required, Validators.minLength(10),]],
      otp: ['', []],
    });

    this.signupform = this.formBuilder.group({
      fullName: ['', []],
      email: ['', []],
      password: ['', []],
      confirmPassword: ['', []],
    });

    this.modalService.onHide.subscribe(() => {
      this.form.reset({otp: '', mobile: '', password: ''}),
      this.logintype = '';
    })

  }

  get f() {
    return this.form.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      keyboard: false,
      ignoreBackdropClick: true,
      backdrop: true
    });
    
  }

  ngOnInit(){ 

  }

  redirecttoforgotpassword(){
    this.modalRef.hide();
    this.router.navigate(['forgotpassword']);
  }

  redirecttoterms(){
    this.modalRef.hide();
    this.router.navigate(['booksection']);
  }

  login(){

   const value = this.form.value;
   const errors = {};
   var controls = ['mobile'];

   if(this.logintype){
     controls = this.logintype == "password" ? [ 'password' , 'mobile']: ['mobile', 'otp'];
   }

    controls.map((v) => {
      let e = this.setrequirederror(value[v]);

      if(v === 'mobile'){
        e = value[v].length < 9 ? {required : true}: null
      }

      if(e){
        errors[v] = e  
      }
    });
  
   this.form.setErrors(errors);

   if(Object.keys(errors).length == 0 ){

    if(this.logintype === 'otp'){

      let data = {otp: value.otp, phnNum: value.mobile};
      let o = this._LS.loginwithotp(data);
      if(!this.user){
       o = this._LS.validatewithotp(data)
      }

      o.subscribe(
        (response: any) => {

          if(response.user){
           // If initial route exist
           var _routeto = localStorage.getItem('_routeto');
           if(_routeto){
             this.router.navigate([_routeto]);
           }else{
            this.router.navigate(['/']);  
            this._LS.logmein(response);
            this.modalRef.hide();
           }
          }else{
            this.modalRef.hide();
            this.openModal(this.signuptemplate);
            this.userId = response.newUserId;
            this.token = response.token;
          }

        }
      );

    }else if(this.logintype == "password"){
      this._LS.userlogin({phnNum: value.mobile, password: value.password}).subscribe(
        (response: any) => {

          // If initial route exist
          var _routeto = localStorage.getItem('_routeto');
          if(_routeto){
            this.router.navigate([_routeto]);
          }else{
            this.router.navigate(['/']);  
          }
          
          this.modalRef.hide();
        }
      );
    }else{
      this._LS.startlogin({phnNum: value.mobile}).subscribe(
        (response: any) => {
          this.user = response.user;
          if(response.user){
            this.logintype = "password";
          }else{
            this.logintype = "otp";
          }
        }
      );
    }
   }


  }

  signup(){
    const value = this.signupform.value;
    const errors = {};

  ['fullName' , 'email' , 'password' , 'confirmPassword'].map((v) => {
    let e = this.setrequirederror(value[v]);
    if(e){
      errors[v] = e  
    }
  });
  
  this.signupform.setErrors(errors);

  if(Object.keys(errors).length == 0 ){
    this._LS.usersavedetails({...value, ...{userId: this.userId, token: this.token}}).subscribe((res: any) => {
      this.modalRef.hide();

      this._LS.logmein(res);
      this.router.navigate(['/']);  

    })
  }
  }

  setrequirederror(value){
    return !value ? {required : true} : null
  }


  signinwithotp(){

    const {mobile} = this.form.value;
    if(mobile.length == 10){
      this._LS.getotp(mobile).subscribe((res) => {
        this.logintype = 'otp';
      })
    }

  }

  onClickk() {
    if (this.showeye) {
      this.passwordtype = 'text';
      this.showeye = false;
    } else {
      this.passwordtype = 'password';
      this.showeye = true;
    }
    
  }

}
