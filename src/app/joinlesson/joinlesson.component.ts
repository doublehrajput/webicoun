import { Component, OnInit, OnDestroy } from '@angular/core';
import RTCClient from '../utils/RTCClient';
import { LoaderService } from '../loader.service';
import { Service2Service } from '../service2.service';
import { Router } from '@angular/router';
import FirebaseService from '../apis/FirebaseService';

const document: any = window.document;

@Component({
  selector: 'app-joinlesson',
  templateUrl: './joinlesson.component.html',
  styleUrls: ['./joinlesson.component.css'],
})
export class JoinLessonComponent implements OnInit, OnDestroy {
  public client;
  public fb;
  public joined = false;
  public date;
  public id;
  public child_added;

  public messages = [];
  public remoteStream;

  videocontrols = {
    play: true,
    sound: true,
  };

  constructor(
    private _loader: LoaderService,
    private _router: Router,
    private _S2S: Service2Service
  ) {
    this.client = new RTCClient();
    this.client.createClient();

    this.fb = new FirebaseService();

    this.id = localStorage.getItem('id');

    this.joinchat();
  }

  subscribe = () => {
    this.client.on('stream-added', (evt) => {
      var remoteStream = evt.stream;

      this.client.subscribe(remoteStream, (err) => {
        console.log('stream subscribe failed', err);
      });
    });

    this.client.on('stream-subscribed', (evt) => {
      this.remoteStream = evt.stream;

      setTimeout(() => {
        this.joined = true;

        var video = document.getElementById('remote-stream');
        video.srcObject = this.remoteStream.stream;
        video.play();
      }, 2000);
    });
  };

  ngOnInit(): void {
    this._loader.show();

    this._S2S.me().subscribe(
      (response: any) => {
        const isSubscribed = response.subscription?.paymentDetails?.paymentId;

        if (isSubscribed) {
          this.subscribe();
        } else {
          this._router.navigate(['/home']);
        }
        this._loader.hide();
      },
      () => {
        this._loader.hide();
      }
    );
  }

  ngOnDestroy() {
    this.fb.db
      .ref('webicoun_chats/' + this.date)
      .off('child_added', this.child_added);
    this.client.leave();
  }

  join = () => {
    this._loader.show();

    this.client
      .join({
        channel: 'webicoun-channel',
      })
      .then(() => {
        this._loader.hide();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  leave = () => {
    this.joined = false;

    this.client
      .leave()
      .then(() => {
        this._router.navigate(['/']);
      })
      .catch(() => {
        this._router.navigate(['/']);
      });
  };

  joinchat = () => {
    this.date = new Date().toISOString().substring(0, 10).replace('T', ' ');

    this.child_added = this.fb.db
      .ref('webicoun_chats/' + this.date)
      .on('child_added', (snapshot) => {
        this.messages.push(snapshot.val());

        this.scrollWindow();
      });
  };

  play = (status) => {
    if (status) {
      this.remoteStream.unmuteVideo();
      this.remoteStream.unmuteAudio();
    } else {
      this.remoteStream.muteVideo();
      this.remoteStream.muteAudio();
    }
  };

  volumechange = ($event) => {
    if ($event.srcElement.muted) {
      this.remoteStream.muteAudio();
    } else {
      this.remoteStream.unmuteAudio();
    }
  };

  sendMessage = (inpM: any) => {
    const value = inpM.value;

    if (value != '') {
      this.fb.db.ref('webicoun_chats/' + this.date).push({
        content: value,
        timestamp: Date.now(),
        name: localStorage.getItem('userName'),
        id: localStorage.getItem('id'),
      });

      inpM.value = '';
    } else {
      alert('Please Enter A Message');
    }
  };

  scrollWindow = () => {
    setTimeout(() => {
      var element = document.getElementsByClassName('chat-box')[0];
      element.scrollTop = element.scrollHeight + 600;
    }, 500);
  };
}
