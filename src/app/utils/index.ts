export * from './AuthenticationInterceptor';

/* export const categories = {
    "mental-wellness-coach" : "Mental Wellness Coach",
    "emotional-wellness-coach" : "Emotional Wellness Coach",
    "physical-wellness-trainers": "Physical Wellness Trainers",
    "nutritionist-health-expert" : "Nutritionist/Health Expert",
    "motivational-speaker" : "Motivational Speaker",
    "business-coach": "Business Coach",
    "personality-development-coach": "Personality Development Coach",
    "career-counsellor" : "Career Counsellor",
    "environmentalist": "Environmentalist",
    "etiquette-coach":"Etiquette Coach",
    "spiritual-speakers": "Spiritual Speakers",
    "happiness-coach": "Happiness Coach",
};
*/
export const categories = {
  "yoga" : "Yoga",
  "career-counsellor":"Career",
  "mental-wellness":"Mental Wellness",
  "emotional-wellness":"Emotional Wellness",
  "personality-development": "Personality Development",
  "happiness-coach": "Happiness",
  "meditation" : "Meditation",
  "motivational-speaker" : "Motivation",
 
  
 };

export function getCategoryName(category){
    return categories[category];
}

export function getCategoryId(name){
    return Object.keys(categories).find(key => categories[key] === name);
}

export const chunk = (arr, size) =>
Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
    arr.slice(i * size, i * size + size)
);