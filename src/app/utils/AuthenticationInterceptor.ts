import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LoginserviceService } from '../loginservice.service';



@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(public toasterService: ToastrService, public router: Router, public _LS: LoginserviceService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('JW Token');

    if (token) {
      const changedReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + token),
      });

      return next.handle(changedReq)
      .pipe(
        tap(),
        catchError((error: any, caught: any) => {
          var ms = "Internal Server Error";
          if(error && error.error && error.error.error){
            ms = error.error.error;
          }
          this.toasterService.error(ms, '', { positionClass: 'toast-top-center' });

          if (error.status === 403) {
            this._LS.logout();
            this.router.navigate(['']);

        }
          return throwError(error)
        }),
      )
      ;
    }

    return next.handle(req).pipe(
      tap(),
      catchError((error: any, caught: any) => {
        this.toasterService.error(error?.error?.error, '', { positionClass: 'toast-top-center' });

        if (error.status === 403) {
          this._LS.logout();
          this.router.navigate(['']);
      }
        return throwError(error)

      }),
    );
  }
}
