import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CndComponent } from './cnd.component';

describe('CndComponent', () => {
  let component: CndComponent;
  let fixture: ComponentFixture<CndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
