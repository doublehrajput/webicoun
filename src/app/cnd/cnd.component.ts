import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { API_BASE_URL } from 'src/environments/environment';

@Component({
  selector: 'app-cnd',
  templateUrl: './cnd.component.html',
  styleUrls: ['./cnd.component.css'],
})
export class CndComponent implements OnInit {
  url = API_BASE_URL + 'videos/career-education ';
  url1;
  webinars: any[] = [];
  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.http
      .get(this.url)
      .toPromise()
      .then((data: any) => {
        this.webinars = data.videos;
        console.log(data);
        for (var index in this.webinars) {
          this.url1 = this.webinars[index].videourl.replace(
            'watch?v=',
            'embed/'
          );
          this.webinars[index].videourl = this.url1;
        }
      });
  }
  getembedurl(url2) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url2);
  }
}
