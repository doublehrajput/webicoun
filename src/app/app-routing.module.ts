import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounsellinggComponent } from './counsellingg/counsellingg.component';
import { WebinarssComponent } from './webinarss/webinarss.component';
import { HomeComponent } from './home/home.component';
import { SocwellnessComponent } from './socwellness/socwellness.component';
import { SprtwellnessComponent } from './sprtwellness/sprtwellness.component';
import { MovtComponent } from './movt/movt.component';
import { EnvtwellnessComponent } from './envtwellness/envtwellness.component';
import { EmtwellnessComponent } from './emtwellness/emtwellness.component';
import { PhyswellnessComponent } from './physwellness/physwellness.component';
import { CndComponent } from './cnd/cnd.component';
import { PdpComponent } from './pdp/pdp.component';
import { SignupComponent } from './signup/signup.component';
import { ExpertsComponent } from './experts/experts.component';
import { BooksectionComponent } from './booksection/booksection.component';
import { LivesessionComponent } from './livesession/livesession.component';
import { PolicyComponent } from './policy/policy.component';
import { FaqComponent } from './faq/faq.component';
import { SigninComponent } from './signin/signin.component';
import { ExpertSignInComponent } from './expert-sign-in/expert-sign-in.component';
import { StudentdashboardComponent } from './studentdashboard/studentdashboard.component';
import { ExpertdashboardComponent } from './expertdashboard/expertdashboard.component';
import { ExpertsignupComponent } from './expertsignup/expertsignup.component';
import { StudentdashnavComponent } from './studentdashnav/studentdashnav.component';
import { Expertsignup2Component } from './expertsignup2/expertsignup2.component';
import { Booksession2Component } from './booksession2/booksession2.component';
import { BlogComponent } from './blog/blog.component';
import { BlogsComponent } from './blogs/blogs.component';
import { ContactusComponent } from './contactus/contactus.component';
import { CareersComponent } from './careers/careers.component';
import { DescriptionblogsComponent } from './descriptionblogs/descriptionblogs.component';
import { Live2Component } from './live2/live2.component';
import { JoinLessonComponent } from './joinlesson/joinlesson.component';
import { HostLessonComponent } from './hostlesson/hostlesson.component';
import { CartFormComponent } from './cart-form/cart-form.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ExpertprofileComponent } from './expertprofile/expertprofile.component';
import { ForgotPasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetPasswordComponent } from './resetpassword/resetpassword.component';
import { ExpertForgotPasswordComponent } from './expertforgotpassword/expertforgotpassword.component';
import { ExpertResetPasswordComponent } from './expertresetpassword/expertresetpassword.component';
import { AcceptPayment } from './accept-payment/acceptpayment.component';
import { PastWebinarsComponent } from './past-webinars/past-webinars.component';
import { PastClassesComponent } from './past-classes/past-classes.component';
import { CourseComponent } from './course/course.component';
import { ExprtprofileComponent } from './exprtprofile/exprtprofile.component';
import { BlogCategoryComponent } from './blog-category/blog-category.component';
import { CommonModule } from '@angular/common';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { TagComponent } from './tag/tag.component';

import {SlickCarouselComponent, SlickItemDirective, CourseCarouselComponent} from './components/slickcarousel/slickcarousel.component';

import {
  Isloggedinguard,
  SessionGuard,
  UserGuard,
  ExpertGuard,
} from './guards';

const routes: Routes = [
  {path:'create-blog', component:CreateBlogComponent},
  { path: 'Counsellingg', component: CounsellinggComponent },
  {path:'exprtprofile',component:ExprtprofileComponent},
 // { path: 'Webinarss', component: WebinarssComponent },
  { path: 'home', component: HomeComponent },
  { path: 'addproduct', component: AddproductComponent },
  { path: '', component: HomeComponent },
  { path: 'cart-form', component: CartFormComponent },
  { path: 'movt', component: MovtComponent },
  { path: 'socwellness', component: SocwellnessComponent },
  { path: 'sprtwellness', component: SprtwellnessComponent },
  { path: 'envtwellness', component: EnvtwellnessComponent },
  { path: 'emtwellness', component: EmtwellnessComponent },
  { path: 'physwellness', component: PhyswellnessComponent },
  { path: 'cnd', component: CndComponent },
  { path: 'pdp', component: PdpComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'experts/:category', component: ExpertsComponent, canActivate: [SessionGuard] },
  { path: 'blog-category/:category', component: BlogCategoryComponent },
  { path: 'tag/:title', component: TagComponent },
  { path: 'booksection', component: BooksectionComponent },
  { path: 'past-webinars', component: PastWebinarsComponent },
  {
    path: 'livesession',
    component: LivesessionComponent,
    canActivate: [SessionGuard],
  },
  { path: 'policy', component: PolicyComponent },
  { path: 'faq', component: FaqComponent },
  {
    path: 'signin',
    component: SigninComponent,
    canActivate: [Isloggedinguard],
  },
  {
    path: 'forgotpassword',
    component: ForgotPasswordComponent,
    canActivate: [Isloggedinguard],
  },
  {
    path: 'resetpassword',
    component: ResetPasswordComponent,
    canActivate: [Isloggedinguard],
  },
  {
    path: 'expertforgotpassword',
    component: ExpertForgotPasswordComponent,
    canActivate: [Isloggedinguard],
  },
  {
    path: 'expertresetpassword',
    component: ExpertResetPasswordComponent,
    canActivate: [Isloggedinguard],
  },
  {
    path: 'expert-sign-in',
    component: ExpertSignInComponent,
    canActivate: [Isloggedinguard],
  },
  {
    path: 'studentdashboard',
    component: StudentdashboardComponent,
    canActivate: [SessionGuard],
  },
  {
    path: 'past-classes',
    component: PastClassesComponent,
    canActivate: [SessionGuard],
  },
  {
    path: 'course/:id',
    component: CourseComponent,
    canActivate: [SessionGuard],
  },
  { path: 'expertdashboard', component: ExpertdashboardComponent },
  { path: 'expertsignup', component: ExpertsignupComponent },
  { path: 'studentdashnav', component: StudentdashnavComponent },
  { path: 'expertsignup2', component: Expertsignup2Component },
  { path: 'booksession2', component: Booksession2Component },
  { path: 'blog', component: BlogComponent },
  { path: 'blogs', component: BlogsComponent },
  { path: 'paynow', component: BlogsComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'careers', component: CareersComponent },
  { path: 'descriptionblogs/:id', component: DescriptionblogsComponent },
  { path: 'live2', component: Live2Component },
  { path: 'accept-payment', component: AcceptPayment },
  {
    path: 'joinlesson',
    component: JoinLessonComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'hostlesson',
    component: HostLessonComponent,
    canActivate: [ExpertGuard],
  },
  {
    path: 'expertprofile/:id',
    component: ExpertprofileComponent,
    canActivate: [SessionGuard],
  },
  {
    path: 'exprtprofile/:id',
    component: ExprtprofileComponent,
    canActivate: [SessionGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const RoutingComponent = [
  CounsellinggComponent,
  WebinarssComponent,
  HomeComponent,
  SocwellnessComponent,
  SprtwellnessComponent,
  MovtComponent,
  PastWebinarsComponent,
  EnvtwellnessComponent,
  EmtwellnessComponent,
  PhyswellnessComponent,
  CndComponent,
  PdpComponent,
  SignupComponent,
  ExpertsComponent,
  BooksectionComponent,
  LivesessionComponent,
  HostLessonComponent,
  PolicyComponent,
  FaqComponent,
  SigninComponent,
  ExpertSignInComponent,
  ExpertdashboardComponent,
  StudentdashboardComponent,
  ExpertsignupComponent,
  StudentdashnavComponent,
  Expertsignup2Component,
  ExprtprofileComponent,
  Booksession2Component,
  ContactusComponent,
  CareersComponent,
  DescriptionblogsComponent,
  JoinLessonComponent,
  BlogComponent,
  CartFormComponent,
  AddproductComponent,
  ExpertprofileComponent,
  BlogsComponent,
  ForgotPasswordComponent,
  ResetPasswordComponent,
  ExpertForgotPasswordComponent,
  ExpertResetPasswordComponent,
  AcceptPayment,
  CreateBlogComponent,
  PastClassesComponent,
  CourseComponent,
  SlickCarouselComponent,
  SlickItemDirective,
  CourseCarouselComponent
];
