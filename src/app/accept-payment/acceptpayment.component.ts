import { Component, OnInit } from '@angular/core';
import { RazorpayService } from '../apis/razorpay';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'accept-payment',
  templateUrl: './acceptpayment.component.html',
  styleUrls: ['./acceptpayment.component.css']
})
export class AcceptPayment implements OnInit {


  public message = "Please wait... we are accepting your payment";

    constructor( private _razor: RazorpayService, private activatedRoute: ActivatedRoute ){

  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.verifyPayment(params);
    });
  }

  verifyPayment = (data) => {
    this._razor.acceptPayment(data).subscribe(
      (response: any) => {
          this.message = response.message;
      },
      (error) => {

      }
    )
  }

}
