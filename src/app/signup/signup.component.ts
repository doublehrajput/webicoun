import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../must-match/must-match.validator';
import { RegisterServiceService } from '../register-service.service';
import { Router } from '@angular/router';
import { MessageService } from '../message.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent {
  registerForm: FormGroup;
  submitted = false;
  successMessage;
  password;
  show = false;

  form = 1;
  otp = '';

  constructor(
    private formBuilder: FormBuilder,
    private myservice: RegisterServiceService,
    private _router: Router,
    private _MSG: MessageService

  ) { }

  ngOnInit() {
    this.password = 'password';
    this.registerForm = this.formBuilder.group(
      {
        fullName: ['', Validators.required],
        number: [
          '',
          [
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(10),
          ],
        ],
        email: ['', [Validators.required, Validators.email]],
        password: [
          '',
          [Validators.required, Validators.pattern, Validators.minLength(4)],
        ],
        confirmPassword: ['', Validators.required],
        dob: ['', Validators.required],
        acceptTerms: [false, Validators.requiredTrue],
      },
      {
        validator: MustMatch('password', 'confirmPassword'),
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit(data) {
    this.submitted = true;
    this.myservice.signup(data).subscribe((response) => {
      this.form = 2;
    });
   
  }
  

  validateUser = () => {
    this.myservice.validateuser({
      otp: this.otp
    }).subscribe((response: any) => {
      this._MSG.show(response.message);
      this._router.navigate(['/signin']);
    });
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

  onClickk() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }
}
