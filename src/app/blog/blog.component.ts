import { Component, OnInit } from '@angular/core';
import { BlogserviceService } from '../blogservice.service';
import { Router } from '@angular/router';
import { Service2Service } from 'src/app/service2.service';
import {chunk} from '../utils/index';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
})
export class BlogComponent implements OnInit {
  constructor(private _service: BlogserviceService, private router: Router,private _S2S: Service2Service) {}

  blogs1: any = [];
  category:any;

  image = [];
  blogs:any=[];
  blogee:any=[];
  bloglist:any=[];
  

  ngOnInit() {
  this._S2S
    .bloglist()
    .subscribe((data: any) => {
      //console.log(JSON.stringify(data));
      //console.log(data.blogcat);
      this.blogs=data.blogcat;
      this.bloglist=data.blogs;
      //console.log(JSON.stringify(data));
      this.blogee = chunk(this.blogs, 8);
   
      //console.log(JSON.stringify(this.new_upcoming));


    });
    this._service
      .list()
      .subscribe((response: any) => (this.blogs1 = response.blogs));
  }

 read(id) {
   // this._service.setBlog(blog);
    this.router.navigate(['/descriptionblogs/'+id]);
  }

  
 clickImg(category)

 {


   this.router.navigate(['/blog-category/' + category])
  /*this._S2S
    .blogbycategory(category)
    .subscribe((data: any) => {
      console.log(JSON.stringify(data));
     
   });
   */

   

  }
}
