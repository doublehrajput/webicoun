import { Component, OnInit } from '@angular/core';
import { Service2Service } from '../service2.service';
import { MessageService } from '../message.service';
import { RazorpayService } from '../apis';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { Router, RouterLinkWithHref } from '@angular/router';
import * as AOS from 'aos';

import { RAZORPAY_OPTIONS } from 'src/environments/environment';

declare let Razorpay: any;

@Component({
  selector: 'app-livesession',
  templateUrl: './livesession.component.html',
  styleUrls: ['./livesession.component.css'],
})
export class LivesessionComponent implements OnInit {

  bsModalRef: BsModalRef;

  pack = '';

  courses = {
    emotional_and_mental_wellness_courses: [],
    career_counselling_courses: [],
    motivational_courses: [],
    personality_development_courses: [],
  };

  constructor(
    private _S2S: Service2Service,
    private router: Router,
    private _MS: MessageService,
    private _razor: RazorpayService,
  ) {
    this.me();
  }

  me(){
    let d = (new Date).getDay() - 1;

    if(d > 4 || d == -1){
      d = 1;
    }
    this._S2S.livelessons().subscribe((response: any) => {

      const {liveclasses} = response[0];
      const {subscription,_id} = response[2];

      this.todayClass = liveclasses[d];      
      this.upcomingClasses = liveclasses.slice(d+ 1, 5).concat(liveclasses.slice(0, d));
      this.pastclasses = response[1].pastclasses;

      this.isSubscribed = subscription.doe ? new Date(subscription.doe) > new Date() ? 1 : 2 : 10;   
      this.subcat = subscription?.category;

      this.profile = {
        _id,
        days: [],
      };
    

      this.courses = response[3].courses;
      
    });  
  }

  descript: any = [];
  isSubscribed;
  subcat;
  profile = {
    _id: false,
    days: [],
  };

  days = [];

  todayClass : any = false;
  upcomingClasses = [];
  upComClasChunk = [];
  pastclasses = [];

  chunk = (arr, size) =>
    Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
        arr.slice(i * size, i * size + size)
  );

  ngOnInit() {
    AOS.init();  
  }

  setRazorPay = (options) => {
    return Object.assign({}, RAZORPAY_OPTIONS, options);
  };

  pay() {

    var pack = this.pack;
    
    if(!pack){
      return ;
    }
    

    this._S2S.purchaseplan(pack).subscribe(
      (response: any) => {
        let op = this.setRazorPay({
          amount: response.amount,
          currency: response.currency,
          order_id: response.id,
          handler: this.razorPaySuccessHandler.bind(this),
        });

        let razorpay = new Razorpay(op);
        razorpay.open();
      },
      () => {
        this._MS.show('Internal Server Error');
      }
    );
  }

  public razorPaySuccessHandler(response) {
    this._razor.purchaseplan(response).subscribe(
      (response) => {
        this._MS.show('Payment Success');
        this.router.navigate(['/livesession']);
      },
      (error) => {
        this._MS.show('Payment Failed');
      }
    );
  }

  openClass = () => {

    var h = new Date().getHours();

    if (19 < h && h < 21) {
      this._S2S.livelessonjoin().subscribe(
        () => {
          this.router.navigate(['/joinlesson']);
        },
        () => {
          this.router.navigate(['/paynow']);
        }
      );
    } else {
      this._MS.show('Class can only open between 7 PM to 8 PM');
    }
  };

  savePreferences = (day) => {
    let d = day - 1,
      is_notify = true;

    if (this.profile.days.indexOf(day) >= 0) {
      this.profile.days = this.profile.days.filter((e) => e !== day);
      is_notify = false;
    } else {
      this.profile.days.push(day);
    }

    this._S2S
      .savenotificationpreferences({
        _id: this.profile._id,
        days: this.profile.days,
      })
      .subscribe((response: any) => {
        this._MS.show(response.message);

        this.descript[d].is_notify = is_notify;
        
      });
  };

  bookfreeclass(){
    
    this._S2S.bookfreeclass().subscribe(() => {
      this.me();
    })
  }

  navigatetocourse(c: any){
    this.router.navigate(['course/' + c._id])
  }

}
