import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MessageService } from '../message.service';

import { Service2Service } from '../service2.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  modalRef: BsModalRef | null;
  srNo = false;
  hidestartcourse = false;

  @ViewChild('template') template;

  videourl;
  certificate = false;
  courseId;
  course = {
    numSessions: 0,
    thumbnail: '',
    description: '',
    title: '',
    videos: []
  };
  constructor(
    private _S2S: Service2Service, private sanitizer: DomSanitizer , 
    private _ARouter: ActivatedRoute,
    private modalService: BsModalService,
    private _MS: MessageService
    ) { }

  ngOnInit() {

    this._ARouter.params.subscribe((params) => {
      this.courseId = params.id;

      this.getVideos();
    });
  }

  getVideos(){
    this._S2S.getcourse({courseId: this.courseId}).subscribe((res: any) => {

      this._S2S.me().subscribe((resme: any) => {
        let course = resme.courses.find((c) => {
          return c.name == this.courseId
        });

        var uptovideowatched = 0;

        if(course){
          this.hidestartcourse = true;
          uptovideowatched = course.videos.length ;
          this.certificate = course.certificate;
        }

          this.certificate = res.certificate;
        //src
        var base = "/assets/";
        res.course.videos.map((r, i) => {
          r.thumbnail = i <= uptovideowatched ? base + 'course-video_active.png' : base + 'course-video.png';
          r.active = i <= uptovideowatched;
          return r;
        });

        this.course = res.course;
        
      })

    });
  }

  getembedurl(url2) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url2);
  }

  playvideo = (id , active , key) =>{

    if(active){
      this._S2S.playvideo(this.courseId, id).subscribe((res: any) => {
        this.videourl = this.getembedurl(res.video.videoUrl);
        this.modalRef = this.modalService.show(this.template);
        key = key + 1;

        if(key == this.course.videos.length){
          this.certificate = true;
        }else{
          this.course.videos[key].active = true;
          this.course.videos[key].thumbnail = "/assets/course-video_active.png";  
        }
        
      })
    }else{
      this._MS.show('Please Watch Videos in cronological order');
    }
   
  }

  startcourse = () => {
    this._S2S.startcourse({courseId: this.courseId}).subscribe((res: any) => {
      this._MS.show("Your Course Has been Started");
      this.srNo = res.sr;

      this.certificate = res.certificate;
      var uptovideowatched = res.sr;
      //src
      var base = "/assets/";
      res.course.videos.map((r, i) => {
        r.thumbnail = i <= uptovideowatched ? base + 'course-video_active.png' : base + 'course-video.png';
        r.active = i <= uptovideowatched;
        return r;
      });

      this.course = res.course;
    });
  }

  getcertificate =() => {
    this._S2S.getcertificate(this.courseId).subscribe((res: any) => {
      var downloadURL = window.URL.createObjectURL(res);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "certificate.pdf";
      link.click()
    })
  } 
}
