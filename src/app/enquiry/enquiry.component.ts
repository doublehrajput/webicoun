import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Service2Service } from '../service2.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.css'],
})
export class EnquiryComponent implements OnInit {
  enquiryForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private _S2S: Service2Service,
    private _MS: MessageService
  ) {
    this.enquiryForm = this.formBuilder.group({
      category: ['', Validators.required],
      message: ['', Validators.required],
    });
  }

  ngOnInit() {}

  submit = () => {
    this.submitted = true;

    if (!this.enquiryForm.valid) {
      return;
    }

    this._S2S.enquiry(this.enquiryForm.value).subscribe((response: any) => {
      this._MS.show(response.message);
    });
  };

  get ef() {
    return this.enquiryForm.controls;
  }
}
