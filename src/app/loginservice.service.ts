import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from './loader.service';
import { finalize } from 'rxjs/operators';
import { API_BASE_URL } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoginserviceService {
  public getLoggedInName = new BehaviorSubject(
    localStorage.getItem('userName')
  );

  logout(): void {
    localStorage.removeItem('userName');
    localStorage.removeItem('JW Token');
    localStorage.removeItem('role');
    localStorage.removeItem('id');
    this.getLoggedInName.next('');
  }

  constructor(private http: HttpClient, private _loader: LoaderService) {
    const userName = localStorage.getItem('userName');
    this.getLoggedInName.next(userName);
  }

  logoutservie(){
    this._loader.show();

    return this.http.post(API_BASE_URL + 'logout', {}).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  login(userName) {
    this.getLoggedInName.next(userName);
  }




  expertlogin = (data) => {
    this._loader.show();

    return new Observable((observer: any) => {
      this.http
        .post(API_BASE_URL + 'expert/signin', data)
        .pipe(
          finalize(() => {
            this._loader.hide();
          })
        )
        .subscribe(
          (success: any) => {
            if(success.isValid === false){
              observer.next(false);
              return ;
            }

            const {
              token,
              expert: { firstName, _id },
            } = success;
            localStorage.setItem('JW Token', token);
            localStorage.setItem('userName', firstName);
            localStorage.setItem('id', _id);
            localStorage.setItem('role', 'expert');

            this.login(firstName);
            observer.next(true);
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  };

  logmein(success){
    const {
      token,
      user: { fullName, _id },
    } = success;
    localStorage.setItem('JW Token', token);
    localStorage.setItem('userName', fullName);
    localStorage.setItem('id', _id);
    localStorage.setItem('role', 'user');

    this.login(fullName);
  }

  logmeexpert(success){
    const {
      token,
      expert: { firstName, _id },
    } = success;
    localStorage.setItem('JW Token', token);
    localStorage.setItem('userName', firstName);
    localStorage.setItem('id', _id);
    localStorage.setItem('role', 'expert');

    this.login(firstName);
  }

  userlogin = (data , type= "signin-password") => {
    this._loader.show();

    return new Observable((observer: any) => {
      this.http
        .post(API_BASE_URL + type, data)
        .pipe(
          finalize(() => {
            this._loader.hide();
          })
        )
        .subscribe(
          (success: any) => {

            if(success.hasOwnProperty('token')){
              const {
                token,
                user: { fullName, _id },
              } = success;
              localStorage.setItem('JW Token', token);
              localStorage.setItem('userName', fullName);
              localStorage.setItem('id', _id);
              localStorage.setItem('role', 'user');
  
              this.login(fullName);
              observer.next(true);
            }else{

              observer.next(false);
            }

          
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  };

  forgotpassword = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'forgotpassword', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };

  resetpassword = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'passwordreset', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };


  getotp = (number) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'signin-otp', {
      phnNo: number
    }).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  startlogin = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'signin', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  loginwithotp = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'validate-signinOTP', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  validatewithotp = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'validateuser', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  loginwithpassword = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'signin-password', data).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }


  usersavedetails = (data) => {
    this._loader.show();

    return this.http.post(API_BASE_URL + 'save-userdetails', data, {
      headers: {
        'Authorization': 'Bearer '+ data.token
      }
    }).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  resendotp(data){
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/resend-otp', data, {
    }).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }

  validateotp(data){
    this._loader.show();

    return this.http.post(API_BASE_URL + 'expert/verify-emailOTP', data, {
    }).pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  }
}


