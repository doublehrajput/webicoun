import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExpertsignupsService } from '../expertsignups.service';
import { MessageService } from '../message.service';

@Component({

  selector: 'app-expertforgotpassword',
  templateUrl: './expertforgotpassword.component.html',
  styleUrls: ['./expertforgotpassword.component.css'],
})
export class ExpertForgotPasswordComponent {
  form: FormGroup;
  show = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private _ES : ExpertsignupsService,
    private _MS: MessageService

  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  get f() {
    return this.form.controls;
  }


  forgotpassword = () => {
    this._ES.forgotpassword(this.form.value).subscribe(
        (response: any) => {
            this._MS.show(response.message);
        }
    )
  }

}
