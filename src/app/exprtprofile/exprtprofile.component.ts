import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NewserviceService } from '../newservice.service';
import { Service2Service } from 'src/app/service2.service';
import { MessageService } from 'src/app/message.service';
import { RazorpayService } from '../apis/razorpay';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getCategoryName } from "../utils";

declare var jQuery: any;

@Component({
  selector: 'app-exprtprofile',
  templateUrl: './exprtprofile.component.html',
  styleUrls: ['./exprtprofile.component.css']
})
export class ExprtprofileComponent implements OnInit {
  @ViewChild('close1') close1: ElementRef;
  @ViewChild('close2') close2: ElementRef;

  expert = {
    firstName: '',
    lastName: '',
    image: '',
    email: '',
    mobileNumber: '',
    category: '',
    subcategory: '',
    rating: 0,
    reviews: [],
    products: [],
    description: '',
    _id: '',
    introVideo: '',
    address1:{
    city:''
    },
    experience: '',
    isNumberValid: '',
    languages: [],
    education: '',
    gallery: [],
  };
  category;

  user: {};
  category_url:any;
  public reviews =  {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
  };

  contactForm: FormGroup;
  ratingForm : FormGroup;
  url:any;
  id:any



  constructor(
    private _expertService: NewserviceService,
    public _S2S: Service2Service,
    private _razor: RazorpayService,
    private _MS: MessageService,
    private _ARouter: ActivatedRoute,
    private _router: Router,
    private formBuilder: FormBuilder,

  ) {
    this._ARouter.params.subscribe((params) => {
     this.id=params.id;

      this.contactForm = this.formBuilder.group({
        message: [],
        name: [],
        mobile: [],
        email: [],
        expertId: [params.id]
      });
      
     // console.log(this.url);
    // this.url='http://localhost:4200/exprtprofile/'+this.id;
    // console.log(this.url);

      this.getExpert(params.id);

      this.ratingForm = this.formBuilder.group({
        rating: [0],
        text: [],
      });


    });

  // localStorage.setItem("_routeto", this.url);
  //  console.log(localStorage.getItem("_routeto"));

   // this._expertService.raiseQuery({category: params.category}).subscribe();

  }

  ngOnInit(): void {
    this._S2S.me().subscribe((response: any) => {
      this.contactForm.setValue({
        name: response.fullName,
        mobile: response.mobileNumber,
        email: response.email,
        message: '',
        expertId: this.expert._id
      }) ;
    })
  }

  getSum(total, num) {
    return total + Math.round(num);
  }

  getExpert(id){
    this._expertService.getExpertProfile(id).subscribe(
      (res: any) => {
        if (res.hasOwnProperty('experts')) {
          this.expert = res.experts;
          this.expert.gallery = res.gallery;

          let sum : any = Object.values(res.analysis).reduce(this.getSum , 0);
          for(let i in res.analysis){
            this.reviews[i] = (res.analysis[i] / sum) * 100;
          }



        } else {
          this._router.navigate(['/experts']);
        }
      },
      () => {
      }
    );
  }

  contact1(){
    //var v = this.contactForm.value;
    //expertId = this.expert._id;
    this._S2S.contactexpert({expertId:this.expert._id,category:this.category_url}).subscribe((res: any) => {
      //this.close1.nativeElement.click();

      this._MS.show(res.message);
    })
  }


    contact(){
    //var v = this.contactForm.value;
    //expertId = this.expert._id;
    this._expertService.contactexpert(this.expert._id).subscribe((res: any) => {
      //this.close1.nativeElement.click();

      this._MS.show(res.message);
    })
  }


  rate = () => {
    this._S2S.rateexpert(this.expert._id, this.ratingForm.value).subscribe((response: any) => {
      this.close2.nativeElement.click();
      this._MS.show(response.message);

      this.getExpert(this.expert._id);
    });
  }

  changerate(r){
    this.ratingForm.controls['rating'].setValue(r);
  }

}
