import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExprtprofileComponent } from './exprtprofile.component';

describe('ExprtprofileComponent', () => {
  let component: ExprtprofileComponent;
  let fixture: ComponentFixture<ExprtprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExprtprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExprtprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
