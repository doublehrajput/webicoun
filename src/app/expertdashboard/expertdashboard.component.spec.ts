import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertdashboardComponent } from './expertdashboard.component';

describe('ExpertdashboardComponent', () => {
  let component: ExpertdashboardComponent;
  let fixture: ComponentFixture<ExpertdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
