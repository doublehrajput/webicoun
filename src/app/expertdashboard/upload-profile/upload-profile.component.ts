import { Component, OnInit, TemplateRef , ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { LoginserviceService } from '../../loginservice.service';
import { MessageService } from '../../message.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-upload-profile',
  templateUrl: './upload-profile.component.html',
  styleUrls: ['./upload-profile.component.css']
})
export class UploadProfileComponent implements OnInit {


  logintype = '';

  form: FormGroup;
  signupform: FormGroup;
  
  modalRef: BsModalRef;
  userId: false;
  token: false;
  user: false;
  @ViewChild('signuptemplate') signuptemplate;

  passwordtype = 'password';
  showeye = true;


  constructor(
    private _LS: LoginserviceService,
    private router: Router,private modalService: BsModalService,private formBuilder: FormBuilder,
    private _MS: MessageService
    ) {
  


  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      keyboard: false,
      ignoreBackdropClick: true,
      backdrop: true
    });
    
  }

  ngOnInit(){ 
  var basic = (<any>$("#main-cropper")).croppie({
  viewport: { width: 300, height: 300, type: 'circle' },
  boundary: { width: 300, height: 300 },
  showZoomer: true,
  url: "http://lorempixel.com/500/400/",
 enableExif: true

});

function readFile(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
     (<any>$("#main-cropper")).croppie("bind", {
        url: e.target.result
      });
      
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".actionUpload input").on("change", function() {
  readFile(this);
});
$(".actionDone").on("click", function() {
  $(".actionDone").toggle();
  $(".actionUpload").toggle();
});

$("#showResult").click(function() {
 (<any>$("#main-cropper"))
    .croppie("result", {
      type: "canvas",
      size: "viewport",
      circle: true
    })
    .then(function(resp) {
      $("#result").attr("src", resp);
    });
});


  }

  


}
