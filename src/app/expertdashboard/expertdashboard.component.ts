import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Service2Service } from 'src/app/service2.service';
import { MessageService } from 'src/app/message.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { RAZORPAY_OPTIONS, API_BASE_URL } from 'src/environments/environment';
import { RazorpayService } from '../apis';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { categories } from 'src/environments/environment';



declare let Razorpay: any;

@Component({
  selector: 'app-expertdashboard',
  templateUrl: './expertdashboard.component.html',
  styleUrls: ['./expertdashboard.component.css'],
})
export class ExpertdashboardComponent implements OnInit {
  editprofie = false;
  @ViewChild('feedfile') feedfile: any;
  @ViewChild('postfile') postile: any;
  @ViewChild('cert') cert: any;
  @ViewChild('panCard') panCard: any;
  @ViewChild('adharCard') adharCard: any;
  @ViewChild('adharCardBack') adharCardBack: any;
  @ViewChild('cancelCheque') cancelCheque: any;


  profileForm: FormGroup;
  url = API_BASE_URL + 'expert/me';
  submitted = false;
  subscription = true;
  bsModalRef: BsModalRef;
  showotp = false;
  urlPan:any;
  city:'';
  urladharfront:any;
  urladharback:any;
  urlcertificate:any;
    lead_count:any;
    noti_count:any;
    changeText: boolean;
  gallery = [];
  reviews = [];
  notifications: {
    leads: 0,
    enquiry: 0,
    total: 0
  };

  public reviewsrate =  {
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
  };

  getSum(total, num) {
    return total + Math.round(num);
  }

  noti = {
    leads: [],
    enquiry: []
  };

  profile = {
    firstName: '',
    lastName: '',
    email: '',
    image: 'https://i.postimg.cc/RFMM0wW7/logo.png',
    products: [],
    category: [],
    mobileNumber: '',
  
    gender: '',
    experience: '',
    description: '',
    dob: '',
    DOB: '',
    address1:{
    street:'',
    locality:'',
    city:'',
    pincode:'',
    state:'',
    country:'',
    },
    
    notifications: [],
    introVideo: '',
    _id: '',
    languages: [],
    gallery: [],
    isNumberValid: false,
    rating: 0,
    education: [],
    verification:{
      adharcard: false
    }
  };


  addnewfeedform: FormGroup;
  uploadpostform: FormGroup;
  mycategories = [];
  visiblecategories = [];

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private _S2S: Service2Service,
    private _MS: MessageService,
    private _router: Router,
    private _razor: RazorpayService,
    private modalService: BsModalService
  ) {  this.changeText = false;}

  ngOnInit() {
    this.profileForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      DOB: ['', Validators.required],
      experience: ['', Validators.required],
      street: ['', Validators.required],
      locality: ['', Validators.required],
      city: ['', Validators.required],
      pincode: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      mobileNumber: ['', Validators.required],
      description: ['', Validators.required],
      gender: ['', Validators.required],
     image: ['', Validators.required],
      category: ['', Validators.required],
      subCategory: ['', Validators.required],
      reviews: ['', Validators.required],
      rating: ['', Validators.required]
    });

    this.addnewfeedform = this.formBuilder.group({
      title: [],
      description: [],
      file: []
    });

    this.uploadpostform = this.formBuilder.group({
      title: [],
      file: []
    });

    for (const key in categories) {
      this.mycategories.push({
        item_id: key,
        item_text: categories[key]
      });
    }

    this.getProfile();

  }

  getProfile = () => {
    this.http.get(this.url).subscribe(
      (response: any) => {
       // console.log(JSON.stringify(response));
        this.visiblecategories = [];
        this.profile = response.expert;
       // this.city=this.profile.address1.city;
       // console.log(JSON.stringify(this.profile));
        this.lead_count=response.lead;
        this.noti_count=response.enquiry;
        this.urlPan=response.expert.verification.pancard;
        //console.log(this.urlPan);
        this.urladharfront=response.expert.verification.adharcard;
        this.urladharback=response.expert.verification.adharcardBack;
        this.urlcertificate=response.expert.certificates.certificate;




        for (const control in this.profileForm.controls) {
          let v = this.profile[control];
          this.profileForm.controls[control].setValue(v);
          

        }
        this.profileForm.controls['street'].setValue(this.profile.address1.street);
        this.profileForm.controls['locality'].setValue(this.profile.address1.locality);
        this.profileForm.controls['city'].setValue(this.profile.address1.city);
        this.profileForm.controls['pincode'].setValue(this.profile.address1.pincode);
        this.profileForm.controls['state'].setValue(this.profile.address1.state);
        this.profileForm.controls['country'].setValue(this.profile.address1.country);

        for (let index = 0; index < this.profile.category.length; index++) {
          const item_id = this.profile.category[index];
          
          this.visiblecategories.push(
          {item_id:this.profile.category[index],
          item_text:categories[item_id]});
        }




        this.profile.notifications.map((n) => {
          if(n.category == 'enquiry'){
            this.noti.enquiry.push(n);
          }else if(n.category == 'lead'){
            this.noti.leads.push(n)
          }
        })

      

       // console.log(JSON.stringify(this.noti.enquiry));
       //  console.log(JSON.stringify(this.noti.leads));
  
        this._S2S.gallery().subscribe((r: any) => {
          this.profile.gallery = r.gallery;
        })

        this._S2S.expertreviews(this.profile._id).subscribe((res: any) => {
          this.reviews = res.reviews;

          let sum : any = Object.values(res.analysis).reduce(this.getSum , 0);
          for(let i in res.analysis){
            this.reviewsrate[i] = (res.analysis[i] / sum) * 100;
          }
        });

      },
      (response) => {
        this._router.navigate(['/expert-sign-in']);
      }
    );
  };

  get f() {
    return this.profileForm.controls;
  }

  editprofile = () => {
    let obj={};
    obj['firstName']=this.profileForm.value.firstName;
    //console.log(this.profileForm.value.firstName)
    obj['lastName']=this.profileForm.value.lastName;
    obj['DOB']=this.profileForm.value.DOB;
    obj['bio']=this.profileForm.value.description;
    obj['street']=this.profileForm.value.street;
    obj['locality']=this.profileForm.value.locality;
    obj['pincode']=this.profileForm.value.pincode;
    obj['city']=this.profileForm.value.city;
    obj['state']=this.profileForm.value.state;
    obj['country']=this.profileForm.value.country;
    obj['gender']=this.profileForm.value.gender;
    obj['experience']=this.profileForm.value.experience;
    //console.log(JSON.stringify(obj));
    //console.log(JSON.stringify(this.profileForm.value))
    this._S2S.edit_expert_profile(obj).subscribe((response: any) => {
      this._MS.show(response.message);
      this.editprofie = false;

      this.getProfile();
    });
  };

  onSubmit(data) {
    // console.log('jrgnjrnfkj');
    // if (this.profileForm.valid) {
    //   this.submitted = true;
    // }
  }
  changeAvatar = (file) => {
    const uploadData = new FormData();
    uploadData.append('profilePic', file[0]);

    this._S2S.uploadprofile(uploadData).subscribe((response: any) => {

      this._MS.show(response.message);
      //console.log(response);
      this.profile.image = response.image;
      this.getProfile();
    });

  };


  deleteproduct = (id) => {
    this._S2S.deletesession(id).subscribe(() => {
      this.getProfile();
    });
  };

  setRazorPay = (options) => {
    return Object.assign({}, RAZORPAY_OPTIONS, options);
  };

  public pay(pack: string) {
    this._S2S.expertplan(pack).subscribe(
      (response: any) => {
        let op = this.setRazorPay({
          amount: response.amount,
          currency: response.currency,
          order_id: response.id,
          handler: this.razorPaySuccessHandler.bind(this),
        });
        let razorpay = new Razorpay(op);
        razorpay.open();
      },
      () => {
        this._MS.show('Internal Server Error');
      }
    );


  }

  public razorPaySuccessHandler(response) {
    this._razor.purchaseexpertplan(response).subscribe(
      (response) => {
        this._MS.show('Payment Success');
        this.getProfile();
      },
      (error) => {
        this._MS.show('Payment Failed');
      }
    );
  }

  readNotification = (id) => {
    this._S2S.readnotification({ notifId: id }).subscribe((res: any) => {
      const {
        user: { name, email, phnNum },
        message,
      } = res.data;

      const initialState = {
        title: 'Notification',
        name,
        email,
        phnNum,
        message,
      };

      this.getProfile();

      this.bsModalRef = this.modalService.show(ModalContentComponent, {
        initialState,
      });
    });
  };

  saveintro = (e) => {

    var file = e.files[0];


  }
  viewPan(){
    var link1 = this.urlPan;
    document.getElementById('myLink1').setAttribute("href",link1);
    //document.getElementById('myLink1').innerHTML = link;
  }

  viewadharfront(){

    var link2 = this.urladharfront;
    document.getElementById('myLink2').setAttribute("href",link2);
    //document.getElementById('myLink1').innerHTML = link;
  }
  viewadharback(){
    var link3 = this.urladharback;
    document.getElementById('myLink3').setAttribute("href",link3);
    //document.getElementById('myLink1').innerHTML = link;
  }
  viewCertificate(){
    var link4 = this.urlcertificate;
    document.getElementById('myLink4').setAttribute("href",link4);
    //document.getElementById('myLink1').innerHTML = link;
  }

  uploadexpertpostdata = (f, t) => {
    var file = f.files[0];

    if (file) {
      const uploadData = new FormData();
      uploadData.append('file', file);
      uploadData.append('tile', t.value);

      this._S2S.uploadexpertpostdata(uploadData).subscribe((response: any) => {
        //console.log(response);
      });
    }

  }

  getOtp(phonnum){
    this._S2S.getExpertMobileOtp({
      phnNum: phonnum.value
    }).subscribe((res) => {
      this.showotp = true;
      this.profile.isNumberValid = true;
    })
  }

  verifyMobile(otp){
    this._S2S.verifyExpertMobile({
      otp: otp.value
    }).subscribe((res: any) => {
      this.showotp = false;
      this._MS.show(res.message);
    })
  }


  uploadIntro($event){
    let file = $event.target.files[0];
    var types = /(\.|\/)(mp4)$/i;

    if (file && types.test(file.type) && file.size < 10485760) {
      const uploadData = new FormData();
      uploadData.append('bioVideo', file);

      this._S2S.uploadexpertbioVideo(uploadData).subscribe((response: any) => {
        this.getProfile();
      });
    }
  }

  onItemSelect($event){
    var value = $event.target.value;

    if(!value){
      return;
    }

    this._S2S.addmorecategory({
      category: value
    }).subscribe((response:any) => {
      console.log(response);
      this._MS.show(response.message);
      this.getProfile();

    });


  }


  addnewfeed(){
    //adharCard Append
    const feedfile = this.feedfile.nativeElement.files;

    var val = this.addnewfeedform.value;
    if (feedfile.length > 0) {
      const uploadData = new FormData();
      uploadData.append('image', feedfile[0]);
      uploadData.append('title', val.title);
      uploadData.append('description', val.description);

      this._S2S.addnewfeed(uploadData).subscribe((response: any) => {
        this._MS.show(response.message);
      });
    }

  }

  addnewpost(){
    const feedfile = this.postile.nativeElement.files;

    var val = this.uploadpostform.value;
    //console.log(val);
    if (feedfile) {

      const uploadData = new FormData();
      uploadData.append('post', feedfile[0]);
      //uploadData.append('title', val.title);

      this._S2S.uploadpost(uploadData).subscribe((response: any) => {
        this._MS.show(response.message);
        this.uploadpostform.reset();

        this.getProfile();
      });
    }

  }


  addlanguage(input){
    var v = input.value;

    if(v){
    this._S2S.addlanguage({language: input.value}).subscribe((response: any) => {
      this.profile.languages.push(input.value);
      this._MS.show(response.message);
      //document.getElementById('#lang').val='';
    });

  }

  }

  addqualification(input){
    var v = input.value;

    if(v){
    this._S2S.addqualification({education: input.value}).subscribe((response: any) => {
      this.profile.education.push(input.value);
      this._MS.show(response.message);
    });
  }
  }


  uploaddocs(){
    const uploadData = new FormData();
    uploadData.append('id' , this.profile._id);

    //adharCard Append
    const adharCard = this.adharCard.nativeElement.files;
    if (adharCard.length > 0) {
      uploadData.append('adharCard', adharCard[0]);
    }else{
      alert('Please Upload Aadhar Card');
      return;
    }

    //cancelCheque Append
   /* const cancelCheque = this.cancelCheque.nativeElement.files;
    if (cancelCheque.length > 0) {
      uploadData.append('cancelCheque', cancelCheque[0]);
    }else{
      alert('Please Upload Cancel Cheque');
      return;
    } */

    //aadhar back Append
    const adharCardBack = this.adharCardBack.nativeElement.files;
    if (adharCardBack.length > 0) {
      uploadData.append('adharCardBack', adharCardBack[0]);
    }else{
      alert('Please Upload Aadhar card back Photo');
      return;
    }

    //cancelCheque Append
    const cert = this.cert.nativeElement.files;
    if (cert.length > 0) {
      uploadData.append('cert', cert[0]);
    }else{
      alert('Please Upload Certificate Photo');
      return;
    }

    this._S2S.uploaddocs(uploadData).subscribe((res: any) => {
     // console.log(JSON.stringify(JSON.stringify(res)));
      this._MS.show(res.message);
      this.getProfile();
    })

  }
  editkycdocs(){
    this._S2S.editKycdocs().subscribe((res: any) => {
      //console.log(JSON.stringify(JSON.stringify(res)));
      this._MS.show(res.message);
      //this.getProfile();
    })
  }




  deletepost(id){

    this._S2S.deletepost({postId: id}).subscribe((res: any) => {
      this.getProfile();
    })
  }

  deletevideo(video){

    this._S2S.deletevideo().subscribe((res: any) => {
      //console.log(res);
      this._MS.show(res.message);
      this.getProfile();
    })
  }



  deletelang(language){

    this._S2S.deletelang({deletelanguage: language}).subscribe((res: any) => {
      this.getProfile();
    });
  }

  deleteeducation(education){
    //console.log({deleteeducation: education});
    this._S2S.deleteeducation({deleteeducation: education}).subscribe((res: any) => {
      this.getProfile();
    });
  }



  deletecategory(category){
    //console.log({deletecat: category});
    this._S2S.deletecategory({deletecat: category}).subscribe((res: any) => {
      console.log(res)
      this.getProfile();
    });
  }
  requesteditprofile(){

    this._S2S.requesteditprofile().subscribe((res: any) => {
      this._MS.show(res.message);

    })
  }

  requesteditwebiner(){
    this._S2S.requesteditwebiner().subscribe((res: any) => {
      this._MS.show(res.message);

    })
  }

}

/**
 *
 */
@Component({
  selector: 'notificationmodal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title pull-left">{{ title }}</h4>
      <button
        type="button"
        class="close pull-right"
        aria-label="Close"
        (click)="bsModalRef.hide()"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      Name: {{ name }}<br />
      Email: {{ email }}<br />
      Mobile: {{ phnNum }}<br />
      <br />
      <br />
      Message: {{ message }}<br />
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" (click)="bsModalRef.hide()">
        Close
      </button>
    </div>
  `,
})
export class ModalContentComponent implements OnInit {
  title: string;
  name;
  email;
  phnNum;
  message: any;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {}
}
