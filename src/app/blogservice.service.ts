import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_BASE_URL, SERVER_BASE_URL } from 'src/environments/environment';
import { LoaderService } from './loader.service';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BlogserviceService {
  constructor(private http: HttpClient, private _loader: LoaderService) {}

  _blog = { title: '', description: '', image: '' ,tags:''};

  setBlog(blog) {
    this._blog = blog;
  }

  getBlog() {
    return this._blog;
  }

  list = () => {
    this._loader.show();

    return this.http.get(API_BASE_URL + 'blogs').pipe(
      finalize(() => {
        this._loader.hide();
      })
    );
  };
}
