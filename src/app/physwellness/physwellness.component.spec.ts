import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhyswellnessComponent } from './physwellness.component';

describe('PhyswellnessComponent', () => {
  let component: PhyswellnessComponent;
  let fixture: ComponentFixture<PhyswellnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhyswellnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhyswellnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
