import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Service2Service } from '../service2.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  form: FormGroup;

  constructor( private formBuilder: FormBuilder, private _MS: MessageService , private _S2S : Service2Service) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: [''],
      message: [''],
    });
  }


  send = () => {  
    this._S2S.contactus(this.form.value).subscribe(
      (response: any) => {
        this._MS.show(response.message)
      }
    )
  }

}
