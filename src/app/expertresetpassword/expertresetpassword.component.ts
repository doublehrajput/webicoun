import { Component, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ExpertsignupsService } from '../expertsignups.service';
import { MessageService } from '../message.service';


@Component({
  selector: 'app-expertresetpassword',
  templateUrl: './expertresetpassword.component.html',
  styleUrls: ['./expertresetpassword.component.css'],
})
export class ExpertResetPasswordComponent {
  
  form: FormGroup;
  password = 'password';
  show = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private _LS : ExpertsignupsService,
    private route: ActivatedRoute,
    private _MS: MessageService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      password: ['', [Validators.required]],
      passwordToken: ['']
    });

    this.route.queryParams.subscribe(params => {   
        if(params.hasOwnProperty('token')){
          this.form.controls.passwordToken.setValue(params.token);
        }
   });
  }
  get f() {
    return this.form.controls;
  }

  onClickk() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }


  resetpassword = () => {
    this._LS.resetpassword(this.form.value).subscribe(
        (response: any) => {
            this._MS.show(response.message)
        }
    )
  }

}
