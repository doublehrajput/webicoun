import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvtwellnessComponent } from './envtwellness.component';

describe('EnvtwellnessComponent', () => {
  let component: EnvtwellnessComponent;
  let fixture: ComponentFixture<EnvtwellnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvtwellnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvtwellnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
