import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Service2Service } from '../service2.service';

@Component({
  selector: 'app-past-classes',
  templateUrl: './past-classes.component.html',
  styleUrls: ['./past-classes.component.css']
})
export class PastClassesComponent implements OnInit {
  classes: any[] = [];
  visibleclasses = [];
  page = 1;
  constructor(private _S2S: Service2Service, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this._S2S
      .pastclasses()
      .subscribe((data: any) => {
        this.classes = data.pastclasses; 

        this.pageChanged({page: 1});
      });
  }

  getembedurl(url2) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url2.match(regExp);

    return this.sanitizer.bypassSecurityTrustResourceUrl('//www.youtube.com/embed/'+ match[2]);
  }

  pageChanged(event){
    this.page = event.page;

    let s = (this.page - 1 ) * 10;
    let e = s + 10;
    this.visibleclasses = this.classes.filter((webinar, index) => {
      return s <= index && index < e;
    })
  }

}
