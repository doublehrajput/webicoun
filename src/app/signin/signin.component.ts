import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginserviceService } from '../loginservice.service';
import { PushNotificationService } from '../apis/pushnotification';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
})
export class SigninComponent implements OnInit {
  url = '';
  loginForm: FormGroup;
  userName: string;
  password;
  show = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private _LS: LoginserviceService,
    private _PN: PushNotificationService
  ) {}

  private changeName(name: string): void {
    this.userName = name;
  }

  ngOnInit() {
    this.password = 'password';
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onReset() {
    this.submitted = false;
    this.loginForm.reset();
  }

  login() {
    this.submitted = true;

    this._LS.userlogin(this.loginForm.value).subscribe(
      () => {
        this.router.navigate(['/']);
        this._PN.push_updateSubscription();
      },
      (error) => {
        this.loginForm.controls.email.setErrors({
          email: error?.error?.error,
        });
      }
    );
  }

  onClickk() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }
}
