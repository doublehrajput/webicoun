import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-cart-form',
  templateUrl: './cart-form.component.html',
  styleUrls: ['./cart-form.component.css']
})
export class CartFormComponent implements OnInit {
  url = "https://webicounindia.herokuapp.com/signup";
  registerForm: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      sessionName: ['', Validators.required],
      platform: ['', Validators.required],
      price: ['', Validators.required],
      sessions: ['', Validators.required]
    },
    );
  }

  get f() { return this.registerForm.controls; }
  onSubmit(data) {
    this.submitted = true;
    console.log(this.registerForm);
    this.http.post(this.url, data).subscribe((response) =>
      console.log("result", response)
    )

  }
}