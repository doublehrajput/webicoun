import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  element;

  constructor() {
    this.element = document.getElementById('myModal');
  }

  show = (message: string) => {
    const msg = document.getElementById('server-message');
    msg.textContent = message;
    this.element.style.display = 'block';
  };
}
