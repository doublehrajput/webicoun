import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SprtwellnessComponent } from './sprtwellness.component';

describe('SprtwellnessComponent', () => {
  let component: SprtwellnessComponent;
  let fixture: ComponentFixture<SprtwellnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SprtwellnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SprtwellnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
