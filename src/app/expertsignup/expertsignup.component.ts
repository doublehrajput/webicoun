import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ExpertsignupsService } from '../expertsignups.service';
import { MessageService } from '../message.service';
import { categories } from 'src/environments/environment'

@Component({
  selector: 'app-expertsignup',
  templateUrl: './expertsignup.component.html',
  styleUrls: ['./expertsignup.component.css'],
})
export class ExpertsignupComponent implements OnInit {
  @ViewChild('profilePic') profilePic: any;
  @ViewChild('resume') resume: any;
  @ViewChild('panCard') panCard: any;
  @ViewChild('adharCard') adharCard: any;
  @ViewChild('cancelCheque') cancelCheque: any;
  password;
  show = false;

  registerForm: FormGroup;
  registerForm2: FormGroup;
  submitted = false;
  id = '';
  form = 1;
  otp = '';

  categories= [];

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private expertService: ExpertsignupsService,
    private _MSG: MessageService
  ) {

    for (const key in categories) {
      this.categories.push({
        id: key,
        text: categories[key]
      });
    }

  }

  ngOnInit() {
    this.password = 'password';
    this.registerForm = this.formBuilder.group({
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      dob: ['', Validators.required],
      profilePic: ['', Validators.required],
      street: ['', Validators.required],
      locality: ['', Validators.required],
      city: ['', Validators.required],
      pincode: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      cat: ['', Validators.required],
      bio: ['', Validators.required],
      subcat: [''],
      experience: ['', Validators.required],
      password: ['', Validators.required],
      resume: ['',],
      number: ['', Validators.required],
      gender: ['Male', Validators.required],
    });

    this.registerForm2 = this.formBuilder.group({
      adharCard: ['', Validators.required],
      panCard: ['', Validators.required],
      cancelCheque: ['', Validators.required],
    });
  }

  get f() {
    const form = this.form === 1 ? this.registerForm : this.registerForm2;
    return form.controls;
  }

  next() {
    const data = this.registerForm.value;
    //console.log(this.registerForm.value);
    const uploadData = new FormData();

    //Appending {data} object to formDate
    Object.keys(data).map((element) => {
      if (!['profilePic', 'resume'].includes(element)) {
        if (element === 'dob' && data[element] instanceof Date) {
          data[element] = data[element].toLocaleDateString();
        }
        uploadData.append(element, data[element]);
      }
    });

    //Profile Pic Append
    const profilePic = this.profilePic.nativeElement.files;
    if (profilePic.length > 0) {
      uploadData.append('profilePic', profilePic[0]);
    }

    //Resume Append
   // const resume = this.resume.nativeElement.files;
   // if (resume.length > 0) {
   //   uploadData.append('resume', resume[0]);
   // }

    this.submitted = true;

    if (this.registerForm.valid) {
      this.submitted = false;


      this.expertService.next(uploadData).subscribe((response: any) => {
        this.form = 2;
      });
    }
  }


  onClickk() {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }


  validateUser = () => {
    this.expertService.validateuser({
      otp: this.otp
    }).subscribe((response: any) => {
      this._MSG.show("Email Verified. Please login into your Webicoun account.");
    });
  }

}
