import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarssComponent } from './webinarss.component';

describe('WebinarssComponent', () => {
  let component: WebinarssComponent;
  let fixture: ComponentFixture<WebinarssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
