import { RAZORPAY_OPTIONS } from './../../environments/environment.prod';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { API_BASE_URL} from 'src/environments/environment';
import { Service2Service } from 'src/app/service2.service';
import { LoginserviceService } from '../loginservice.service';
import {chunk} from '../utils/index';

import { MessageService } from '../message.service';
import { RazorpayService } from '../apis';


declare let Razorpay: any;

@Component({
  selector: 'app-webinarss',
  templateUrl: './webinarss.component.html',
  styleUrls: ['./webinarss.component.css'],
})
export class WebinarssComponent {
  url = API_BASE_URL + 'webinars';
  url1;
  role: string = '';
  new_upcoming:any[];
  webinars= {
    'past': [],
    upcoming: []
  };

  past = [];
  upcoming = [];
  constructor(private http: HttpClient,private _ls: LoginserviceService, private sanitizer: DomSanitizer,private _S2S: Service2Service,private _MS: MessageService,
    private _razor: RazorpayService) {}

  ngOnInit() {

    this._S2S
    .upcomingWebinars()
    .subscribe((data: any) => {
      //console.log(JSON.stringify(data));
      this.new_upcoming=data['webinars'];
      //console.log(JSON.stringify(this.new_upcoming));


    });
    this._ls.getLoggedInName.subscribe((name: any) => {
      //this.userName = name;
      this.role = localStorage.getItem('role');
    });
    this.http
      .get(this.url)
      .toPromise()
      .then((data: any) => {
        data.webinars.map((webinar) => {
          if(webinar.category == 'older'){
            if(this.webinars.past.length < 6){
              this.webinars.past.push({url: this.getembedurl(webinar.webiurl)})
            }
          }else{
            this.webinars.upcoming.push({url: webinar.webiurl});
          }

        });

        this.past = chunk(this.webinars.past, 3);
        this.upcoming = chunk(this.webinars.upcoming, 3);

      });
  }

  getembedurl(url2) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url2.match(regExp);

    return this.sanitizer.bypassSecurityTrustResourceUrl('//www.youtube.com/embed/'+ match[2]);
  }

  setRazorPay = (options) => {
    console.log("error");
    //console.log(JSON.stringify(RAZORPAY_OPTIONS));
    //console.log(RAZORPAY_OPTIONS.order_id);
    if(RAZORPAY_OPTIONS.order_id){
          return Object.assign({}, RAZORPAY_OPTIONS, options);
    }
    else{
      this._MS.show('Already registered');
    }
  };

  pay(data) {


    this._S2S.webinarPack(data).subscribe(
      (response: any) => {
        let op = this.setRazorPay({
          amount: response.amount,
          currency: response.currency,
          order_id: response.id,
          handler: this.razorPaySuccessHandler.bind(this),
        });

        let razorpay = new Razorpay(op);
        razorpay.open();
      },
      () => {
        this._MS.show('Internal Server Error');
        console.log("error");
      }
    );
  }

  public razorPaySuccessHandler(response) {
    console.log("pagal");
    this._razor.webinarPack(response).subscribe(
      (response) => {
        this._MS.show('Payment Success');
        //this.router.navigate(['/livesession']);
      },
      (error) => {
        this._MS.show('Payment Failed');
      }
    );
  }

}
