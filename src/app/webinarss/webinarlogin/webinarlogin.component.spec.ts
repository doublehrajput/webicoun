import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarloginComponent } from './webinarlogin.component';

describe('WebinarloginComponent', () => {
  let component: WebinarloginComponent;
  let fixture: ComponentFixture<WebinarloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
